# RPG Mastering Tutor

O projeto visa criar uma plataforma web para auxiliar jogadores de RPG inexperientes com a tarefa de mestrar um jogo
Será possível: visualizar o mapa, obter sugestões de como avançar no jogo, modificar uma história para se encaixar no seu modo de jogo entre outras funcionalidades.

# Instruções
O projeto não precisa de um Makefile, pois é uma aplicação web.

[Link do Website](https://rpg-master-d3327.web.app/) este link é da página já em funcionamento com tudo o que foi implementado até agora.

[Link para o Vídeo](https://youtu.be/9si8dg8SWXU) este link contém um  vídeo com uma breve descrição de cada funcionalidade já implementada do projeto, tais como visualização prévia de história, pesquisa de história, cadastro no banco de dados, login, integração com banco de dados através do firebase.



# Arquitetura
Um estilo de arquitetura adotado foi o modelo de cliente servidor, onde o servidor provê paginas Web aos clientes.
Outro modelo adotado foi o de Camadas, onde uma camada visual (HTML/CSS) recorre à um camada lógica (JavaScript) que 
por sua vez recorre à um camada de Data Access Object (JavaScritp/Firebase).

## Padrões de Projeto Adotados
Escolhemos ultilizar o modelo "Data Access Object" para centralizar as requisições ao banco de dados, separando assim essa responsabilidade. Desse modo é mais simples compreender que acessos são feitos ao Banco de Dados, facilitando a resolução do possíves bugs


## Diagramas
### Diagrama de contexto (C1)
<img src="Diagramas de Arquitetura/C1.png" width="16%"/>

### Diagrama de container (C2)
<img src="Diagramas de Arquitetura/C2.png" width="75%"/>

### Diagrama de componente de Data Access Object (C3)
* Usuário Data Access Object
    * Esse componente JavaScript deve gerenciar todos os acessos de dados relacionados ao cadastro, ao login,
à atualização de perfil dos usuarios.
<br>

* História Data Access Object
    * Esse componente JavaScript deve gerenciar todos os acessos de dados relacionados ao cadastro de novas histórias e à suas leituras.
<br>

* Partida Data Access Object
    * Esse componente JavaScript deve gerenciar todos os acessos de dados relacionados à criação, atualização e deleção de uma Partida.
<br>

* DataBase
    * Esse componente é um banco de dados servidor do Firebase (Real Time Database), onde armazena e provê todos os dados da aplicação.

<br>
<br>
<img src="Diagramas de Arquitetura/C3 - DAO.png" width="45%"/>

### Diagrama de componente da aplicação MultiPagina (C3)
* Template System
    * Esse componente JavaScript contém/provê todos os componentes HTML criados e usados na aplicação. Ex. Header, Footer, ETC.
<br>

* Web Application
    * Esse componente é um servidor do Firebase, no qual armazena e provê as páginas da aplicação.
<br>

* HTML Pages
    * Esses componentes HTML/CSS são responsáveis pelo visual da aplicação.
<br>

* JavaScript Files
    * Esses componentes JavaScript são responsáveis pela lógica da aplicação.

<br>
<br>
<img src="Diagramas de Arquitetura/C3 - MultiPagina.png" width="90%"/>


