describe('SignUp', () => {
    let UserDAO = require('../../public/DAOs/UserDAO');
    const { getFirebaseInstance } = require("../helpers/firebase")
    this.firebase = null;

    const UserVal = {};
    const UserInval = {};

    beforeAll(() => {
        this.firebase = getFirebaseInstance();
        UserDAO = UserDAO.UserDAO;

        UserInval.nome  = "";
        UserInval.email = "XXXXXX@XXXXXX.XXXXXX";
        UserInval.senha = "8765";

        UserVal.nome  = "UsuarioTeste";
        UserVal.email = "abcdefghi@mail.com";
        UserVal.senha = "12345678";
    });

    it('Deve cadastrar com os dados corretos', (done) => {
        UserDAO.signUp.call(this, UserVal.nome, UserVal.email, UserVal.senha, UserVal.senha)
            .then( _ => done())
            .catch( err => done.fail(err));
    });

    it('Deve falhar se o email já estiver cadastrado', async (done) => {
        try {
            await UserDAO.signUp.call(this, UserVal.nome, UserVal.email, UserVal.senha, UserVal.senha);
            done.fail();
        }
        catch (_) {
            const user = this.firebase.auth().currentUser.uid;
            this.firebase.database().ref('usuarios/'+user).remove();

            await this.firebase.auth().signInWithEmailAndPassword(UserVal.email, UserVal.senha);
            await this.firebase.auth().currentUser.delete();

            done();
        }

    });

    it('Deve falhar se o nome for nulo', (done) => {
        UserDAO.signUp.call(this, UserInval.nome, UserVal.email, UserVal.senha, UserVal.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    describe("Deve verificar o formato do Email", () => {

        it('falhando apenas com arroba', async (done) => {
            await UserDAO.signUp.call(this, UserVal.nome, "@", UserVal.senha, UserVal.senha)
                .then(_ => done.fail())
                .catch(_ => done());
        });

        it('falhando apenas com letras', async (done) => {
            await UserDAO.signUp.call(this, UserVal.nome, "xxx", UserVal.senha, UserVal.senha)
                .then( _ => done.fail())
                .catch( _ => done());
        });

        it('falhando sem o arroba', async (done) => {
            await UserDAO.signUp.call(this, UserVal.nome, "xx.x", UserVal.senha, UserVal.senha)
                .then( _ => done.fail())
                .catch(_ => done());
        });

        it('falhando sem o ponto', async (done) => {
            await UserDAO.signUp.call(this, UserVal.nome, "x@xx", UserVal.senha, UserVal.senha)
                .then( _ => done.fail())
                .catch(_ => done());
        });

        it('falhando com ordem incorreta', (done) => {
            UserDAO.signUp.call(this, UserVal.nome, "x.x@x", UserVal.senha, UserVal.senha)
                .then( _ => done.fail())
                .catch( _ => done());
        });
    });

    it('Deve falhar com senha menor que 8 digitos', (done) => {
        UserDAO.signUp.call(this, UserVal.nome, UserVal.email, UserInval.senha, UserVal.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    it('Deve falhar se a senha e a confirmação divergirem', (done) => {
        UserDAO.signUp.call(this, UserVal.nome, UserVal.email, UserVal.senha, UserInval.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    afterAll(() => {
        //Apagar do banco
    });

});