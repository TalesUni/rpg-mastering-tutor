describe('Logout', () => {
    let UserDAO = require('../../public/DAOs/UserDAO');
    const { getFirebaseInstance } = require("../helpers/firebase");
    this.firebase = null;

    const UserVal = {};
    const UserInval = {};
    let user;

    beforeAll( async () => {
        this.firebase = getFirebaseInstance();
        UserDAO = UserDAO.UserDAO;

        UserInval.email = "XXXXXX@XXXXXX.XXXXXX";
        UserInval.senha = "87654321";

        UserVal.email = "abcdefghi@mail.com";
        UserVal.senha = "12345678";
        await UserDAO.signUp.call(this, "UsuarioTeste", UserVal.email, UserVal.senha, UserVal.senha);
        user = this.firebase.auth().currentUser.uid;
    })

    it("deve deslogar um usuário previamente logado", async (done) =>{
        await this.firebase.auth().signInWithEmailAndPassword(UserVal.email, UserVal.senha);

        await UserDAO.logout.call(this, UserVal.email)
            .then( _ => done())
            .catch( err => done.fail(err));

    });

    it("deve falhar se não existir usuário logado", async (done) =>{
        await UserDAO.logout.call(this, UserInval.email)
            .then( _ => done.fail())
            .catch( err => done(err));
    });

    it("deve falhar com email incorreto", async (done) =>{
        await this.firebase.auth().signInWithEmailAndPassword(UserVal.email, UserVal.senha);
        await UserDAO.logout.call(this, UserInval.email)
            .then( _ => done.fail())
            .catch( err => done(err));

        await UserDAO.logout.call(this, UserVal.email);
    });

    afterAll(async () => {
        this.firebase.database().ref('usuarios/'+user).remove();

        await this.firebase.auth().signInWithEmailAndPassword(UserVal.email, UserVal.senha);
        await this.firebase.auth().currentUser.delete();
    });
});