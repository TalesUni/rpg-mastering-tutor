describe('Login', () => {
    let UserDAO = require('../../public/DAOs/UserDAO');
    const { getFirebaseInstance } = require("../helpers/firebase");
    this.firebase = null;

    const UserVal = {};
    const UserInval = {};

    beforeAll( async () => {
        this.firebase = getFirebaseInstance();
        UserDAO = UserDAO.UserDAO;

        UserInval.email = "XXXXXX@XXXXXX.XXXXXX";
        UserInval.senha = "87654321";

        UserVal.email = "abcdefghi@mail.com";
        UserVal.senha = "12345678";

        await UserDAO.signUp.call(this, "UsuarioTeste", UserVal.email, UserVal.senha, UserVal.senha);
    })

    it('Deve aceitar Email e Senha Corretos', (done) => {
        UserDAO.login.call(this, UserVal.email, UserVal.senha)
            .then( _ => done())
            .catch( err => done.fail(err));
    });

    it('Deve falhar se o Email for Incorreto', (done) => {
        UserDAO.login.call(this, UserInval.email, UserVal.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    it('Deve falhar se a Senha for Incorreta', (done) => {
        UserDAO.login.call(this, UserVal.email, UserInval.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    it('Deve falhar se o Email e Senha forem Incorretos ', (done) => {
        UserDAO.login.call(this, UserInval.email, UserInval.senha)
            .then( _ => done.fail())
            .catch( _ => done());
    });

    afterAll(async () => {
        const user = this.firebase.auth().currentUser.uid;
        this.firebase.database().ref('usuarios/'+user).remove();

        await this.firebase.auth().signInWithEmailAndPassword(UserVal.email, UserVal.senha);
        await this.firebase.auth().currentUser.delete();
    });
});