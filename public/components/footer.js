class Footer extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
      <footer class="page-footer font-small foot">

        <div>
            <div class="container-fluid">

                <!-- Grid row-->
                <div class="row py-4 d-flex align-items-center">

                    <!-- Grid column -->
                    <div class="col-md-7 col-lg-5 col-xl-4 text-center text-md-left  mb-md-0 small">
                        <nav class="nav justify-content-center justify-content-md-start">
                            <a class="nav-link text-muted" target="_blank" href="">Privacy Terms</a>
                            <a class="nav-link text-muted" target="_blank" href="">Cookies</a>
                            <a class="nav-link text-muted" target="_blank" href="">About</a>
                        </nav>
                    </div>
                    <!-- Grid column -->
                    <div class="col-md-3 col-lg-4 col-xl-6 text-center text-md-left  mb-md-0 small">
                        <div class="footer-copyright text-center white-text pt-3 pb-2">Developed by Group X @2020
                        </div>
                    </div>


                    <!-- Grid column -->
                    <div class="col-md-2 col-lg-3 col-xl-2 text-center text-md-right">
                        <a href="https://www.facebook.com" target="_blank" class="text-dark">
                            <i class="fab fa-facebook-f white-text mx-2"> </i></a>
                        <a href="https://twitter.com" target="_blank" class="text-dark">
                            <i class="fab fa-twitter white-text mx-2"> </i></a>
                        <a href="https://www.instagram.com" target="_blank" class="text-dark">
                            <i class="fab fa-instagram white-text mx-2"> </i></a>



                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row-->

            </div>
        </div>
      </footer>
    `;
  }
}

customElements.define('footer-component', Footer);