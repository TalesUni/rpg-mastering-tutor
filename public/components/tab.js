class Tab extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
      <title>RPG Tutoring Master</title>
      <link rel="shortcut icon" href="./img/logo.png" />
    `;
  }
}

customElements.define('tab-component', Tab);