// Web app's Firebase configuration
let firebaseConfig = {
  apiKey: "AIzaSyCRZ-WA-uefKE8Jq0OpmeYgu9ZFNmDDlHQ",
  authDomain: "rpg-master-d3327.firebaseapp.com",
  databaseURL: "https://rpg-master-d3327.firebaseio.com",
  projectId: "rpg-master-d3327",
  storageBucket: "rpg-master-d3327.appspot.com",
  messagingSenderId: "906543022280",
  appId: "1:906543022280:web:d64d18e5f32f1240fe25fd",
  measurementId: "G-TXEFVB6BR7"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
