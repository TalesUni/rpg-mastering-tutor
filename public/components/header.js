class Header extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
        <nav id="header-cmp" class="navbar navbar-expand-lg navbar-dark">
          <div class="container-fluid">
              
              
              <a class="navbar-brand" href="Index.html" style="font-size: 32px;">
                <img src="./img/logo.png" href="#" style=" margin-right:20px;" height=80 >
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav nav-fill">
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="biblioteca.html">Biblioteca</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="sobre.html">Sobre</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                     
                  </ul>
                  <ul class="navbar-nav ml-auto ">
                      <li class="nav-item">
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light"  href="Login.html">Login</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light"  href="Signup.html">Cadastrar-se</a>
                      </li>
                     
                  </ul>
              </div>
          </div>
        </nav>
      `;
    }
    
}
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
     document.getElementById("header-cmp").innerHTML = `
            <div class="container-fluid"> 
              <a class="navbar-brand" href="Index.html" style="font-size: 32px;">
                <img src="./img/logo.png" href="#" style=" margin-right:20px;" height=80 >
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav nav-fill">
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="biblioteca.html">Biblioteca</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="novaHistoria.html">Criar História</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="sobre.html">Sobre</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                     
                  </ul>
                  <ul class="navbar-nav ml-auto ">
                      <li class="nav-item">
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light"  href="perfil.html">Perfil</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light" onclick="logout()" href="index.html">Logout</a>
                      </li>
                     
                  </ul>
              </div>
          </div>
      `;
  } else {
    document.getElementById("header-cmp").innerHTMLL = `
          <div class="container-fluid">
              
              
              <a class="navbar-brand" href="Index.html" style="font-size: 32px;">
                <img src="./img/logo.png" href="#" style=" margin-right:20px;" height=80 >
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav nav-fill">
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="biblioteca.html">Biblioteca</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="sobre.html">Sobre</a>
                      </li>
                      <li class="nav-item">
                          <a class="h4 nav-link text-light"  href="#"></a>
                      </li>
                     
                  </ul>
                  <ul class="navbar-nav ml-auto ">
                      <li class="nav-item">
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light"  href="Login.html">Login</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link text-light"  href="Signup.html">Cadastrar-se</a>
                      </li>
                     
                  </ul>
              </div>
          </div>
      `;
  }
});
function logout(){
  firebase.auth().signOut();
}
customElements.define('header-component', Header);
