let idHistoria;

async function show(data) {
    var nome = document.getElementById("title");
    nome.textContent = "Avaliando \"" + await HistoryDAO.getName(idHistoria) + "\"";
}

function atualizar(val) {
    let myratings = document.getElementById("myratings");

    if (val < 2) {
        myratings.style.color = 'red'; 
    } else if (val >= 4){ 
        myratings.style.color = 'green'; 
    } else {
        myratings.style.color = 'orange'; 
    }

    myratings.textContent = val; 
}

async function avaliar() {
    let nota = document.getElementById("myratings");
    await HistoryDAO.updateRate(idHistoria, parseFloat(nota.textContent));
    await MatchDAO.delSave(firebase.auth().currentUser.uid, idSave);
    alert("Historia avaliada! Obrigada por jogar com a gente!");
    window.location.href = './perfil.html?';
}

firebase.auth().onAuthStateChanged(async function(user) {
    if (user) {
        let res = window.location.href.split('?'); 
        if (res[2] != undefined) {
            idHistoria = res[1];
            idSave = res[2];
            await show();
        } else {
            window.location.href = "./perfil.html";
        }
    } else {
        window.location.href = "./login.html";
    }
});