function showData(data) {
    var nome = document.getElementById("nome");
    nome.value = data.nome;

    var email = document.getElementById("email");
    email.value = data.email;

    var sobre = document.getElementById("sobre");
    if (data.sobre != undefined){
        sobre.value = data.sobre;
    } else {
        sobre.setAttribute("placeholder","Escreva aqui uma descricao sobre voce!");
    }
}

async function showSaves(saves) {
    if (saves[0] == undefined) {
        let saves = document.getElementById("saves");
        let card = document.createElement('div');

        card.className = "card text-center p-3";
        card.textContent = "Nenhum save encontrado";

        saves.appendChild(card);
    } else {
        for (let i=0; i<saves.length; i++) {
            var div = document.getElementById("saves");
            var card = document.createElement('div');
            card.className = "card";
            card.id = saves[i].id;

            var cardHeader = document.createElement('div');
            cardHeader.className = "card-header";

            var h4 = document.createElement('h2');
            h4.className = "mb-0 d-flex";

            var btnCollapse = document.createElement('button');
            btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none";
            btnCollapse.textContent = await HistoryDAO.getName(saves[i].idHistoria);
            btnCollapse.setAttribute("onclick","paginaSave('"+saves[i].id+"')");

            let del = document.createElement("button");
            del.setAttribute("type","button");
            del.setAttribute("aria-label","Close");
            del.setAttribute("onclick","delSave('"+saves[i].id+"')");
            del.className = "close";

            let span = document.createElement("span");
            span.setAttribute("aria-hidden","true");
            span.textContent = "x";

            // Add items to the card
            del.appendChild(span);
            h4.appendChild(btnCollapse);
            h4.appendChild(del);
            cardHeader.appendChild(h4);
            card.appendChild(cardHeader);

            // Add card to de page
            div.appendChild(card);
        }
    }
}

async function showMyRPG(minhas) {
    if (minhas[0] == undefined) {
        var elements = document.getElementById("minhas");
        var card = document.createElement('div');

        card.className = "card text-center p-3";
        card.textContent = "Nenhuma criacao encontrada";

        elements.appendChild(card);
    } else {
        for (let i=0; i<minhas.length; i++) {
            var div = document.getElementById("minhas");
            var card = document.createElement('div');
            card.className = "card";

            var cardHeader = document.createElement('div');
            cardHeader.className = "card-header";

            var h4 = document.createElement('h2');
            h4.className = "mb-0";

            var btnCollapse = document.createElement('button');
            btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none";
            btnCollapse.textContent = await HistoryDAO.getName(minhas[i].idHistoria);
            btnCollapse.setAttribute("onclick","paginaHistoria('"+minhas[i].idHistoria+"')");

            // Add card to de page
            h4.appendChild(btnCollapse);
            cardHeader.appendChild(h4);
            card.appendChild(cardHeader);

            // Add card to de page
            div.appendChild(card);
        }
    }
}

async function updateData() {
    let sobre = document.getElementById("sobre");
    let nome = document.getElementById("nome");

    if ((sobre.textContent == "") || (nome.textContent == "")) {
        alert("Nao e possivel atualizar nome ou sobre mim para nulo!");
    } else {
        let id = await firebase.auth().currentUser.uid;
        await UserDAO.updateAbout(id, sobre.value);
        await UserDAO.updateName(id, nome.value);
    }
}

async function paginaSave(id) {
    let save = await MatchDAO.getSaveById(firebase.auth().currentUser.uid, id);
    window.location.href = './partida.html?'+save.idHistoria+"?"+id;
}

async function delSave(id) {
    await MatchDAO.delSave(firebase.auth().currentUser.uid, id);

    let save = document.getElementById(id);
    save.remove();

    // If there is no elements in the page at the end
    let saves = document.getElementById("saves");
    if (saves.childNodes.length == 0) {
      let card = document.createElement('div');

      card.className = "card text-center p-3";
      card.textContent = "Nenhum save encontrado";

      saves.appendChild(card);
    }
}

function paginaHistoria(id) {
    window.location.href = './historia.html?'+id;
}

firebase.auth().onAuthStateChanged(async function(user) {
    if (user) {
        let data = await UserDAO.getUserById(user.uid);
        showData(data);
        showSaves(await UserDAO.getAllSaves(user.uid));
        showMyRPG(await UserDAO.getMyRPGs(user.uid));
    } else {
        window.location.href = "./index.html";
    }
});
