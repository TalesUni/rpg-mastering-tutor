//Variaveis Globais
var data;
let userId;
let idHistoria = null;
let contadorCenasLigadas = 0;
let pai = [];

//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


/*      loadPage()
*       Carrega os dados passados pelos usuario da primeira pagina de criação de história
*/
function loadPage(){
    data = localStorage.getItem("novaHistoria");
    const cenas = JSON.parse(data).cenas;
    let item;
    let titulo_cena;

    for (let i = 0; i < cenas.length; i++) {
        if (i === 0) {
            titulo_cena = cenas[i].titulo + " (Obrigatória)";
        } else {
            titulo_cena = cenas[i].titulo;
        }
        item = `
            <div class="card mb-2 mt-2" id="cena_` + i + `">
                <button class="btn btn-sm col-xl-11 " data-toggle="collapse" data-target="#collapse` + i + `" aria-expanded="true" aria-controls="collapse` + i + `">
                    <div class="form-group" > 
                        <h5 class="mb-1 text-center"> ` + titulo_cena + `  </h5>              
                    </div> 
                </button>           
            </div>
            <div id="collapse` + i + `" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body row"> 
                <div class="col">
                    <div class="display-block mb-3 text-center">                       
                        <span class="input-group-text h-100 justify-content-center" >Cenas disponíveis:</span>                      
                        
                        <div class="card-body border text-center" id="cenas_disponiveis_` + i + `">

                        </div>
                    </div>    

                </div>
            </div>        

        `
        document.getElementById("item").insertAdjacentHTML('beforeend', item);

        let disp;
        for (let x = 0; x < cenas.length; x++) {
            if (i === 0) disp = "";
            else disp = "none";

            if (cenas[x].titulo !== cenas[i].titulo) {

                let html_especial =
                `
                    <div class="col text-left" style="display:` + disp + `;" cena_seletor="` + x + `">
                        <input class="form-check-input" type="checkbox" value="` + x + `"  name="cena_` + x + `"   onchange="atualizar(this,` + i + `,` + x + `)" >
                        <label class="form-check-label mr-2 ml-1" for="` + x + `">
                            ` + cenas[x].titulo + `
                        </label>         
                    </div>
                `
                document.getElementById('cenas_disponiveis_' + i).insertAdjacentHTML('beforeend', html_especial);
            }
        }
    }
    
    Global.main(null,  "canvasGrafo", 320, JSON.parse(data).cenas);
    //carrego o vetor de pais
    for(let x in JSON.parse(data).cenas){
        pai.push(-1);
    }    

}

function atualizar(elemento, indiceOrigem, indiceDestino){

    let auxData = JSON.parse(data);

    if(elemento.checked){
        auxData.cenas[indiceOrigem].proximo.push(indiceDestino);
    }else{
        let pos = auxData.cenas[indiceOrigem].proximo.indexOf(indiceDestino);
        auxData.cenas[indiceOrigem].proximo.splice(pos, 1);
    }
    data = JSON.stringify(auxData);

    //Atualizando grafo
    const cenas = JSON.parse(data).cenas;

    const canvas = document.getElementById('canvasGrafo');
    const canvasClone = canvas.cloneNode(true);
    canvas.parentNode.replaceChild(canvasClone, canvas);

    let count = 0;
    for (const cena of cenas) {
        cena.id = count;
        count ++;
    }

    (function indexaDescendentes(cena, descendentes, cenas){
        if (cena.descendentes === undefined){
            cena.descendentes = [...descendentes];
        }
        else {
            cena.descendentes = cena.descendentes.concat([...descendentes])
        }

        descendentes.push(cena.id);
        for (const prox of cena.proximo) {
            indexaDescendentes(cenas[prox], [...descendentes], cenas);
        }
    })(cenas[0], [], cenas);

    for (const cena of cenas) {
        if (cena.id === 0) continue;

        const elem = document.getElementById("cenas_disponiveis_"+ cena.id);
        if (cena.descendentes === undefined){
            for (const child of elem.children) {
                child.style.display = "none";
            }
        }
        else {
            let childId;
            for (const child of elem.children) {
                childId = parseInt(child.attributes.cena_seletor.value);

                if(! cena.descendentes.includes(childId)) {
                    child.style.display = "";
                }
                else {
                    child.style.display = "none";
                }
            }
        }
    }

    Global.main(null, "canvasGrafo", parseInt(320), JSON.parse(data).cenas);
}


/*      getId(string)
*  Entrada: uma string, que o id de um dos inputs da página, id="STRING_NUMERO" ex id="max_1"
*  Saida: retorna o número presente na string, número que de acordo com a formação inicial estara na ultima posição da string
*/
function getId(string){
    var retorno = string.split("_");
    return retorno[retorno.length-1];
}

/*      finish()
*  Entrada: -
*  Saida: verifica as cenas que foram como próxima de cada cena, e cria o JSON para salvar no banco de Dados
*/
async function finish(){
    let auxData = JSON.parse(data); //por algum motivo não consegui salvar direto sobre data, portanto criei uma variável auxiliar
    var numeroCenas = JSON.parse(data).cenas.length;
    
    
    for(x=0; x < numeroCenas; x++){
        let proximos = [];        
        //percorro os elementos que mostram cenas disponiveis
        let el = document.getElementById('cenas_disponiveis_'+x);        
        //console.log('cenas_disponiveis_'+x);
        for(y=0; y < numeroCenas-1; y++){
            //verifico se os elementos dentro de cenas disponíves estão visiveis, se sim eu verifico se o input filho é true
            if(el.children[y].style.display == ""){
                let inputInterno = el.children[y].children[0].checked;
                if(inputInterno){
                    proximos.push(parseInt(el.children[y].attributes.cena_seletor.value));
                }                
                //console.log(inputInterno);
                //console.log(el.children[y].attributes.cena_seletor.value);
            }
        }
        auxData.cenas[x].proximo = proximos;        
        
    }
    //console.log(auxData);            
    //console.log(auxData.cenas);        
    data = auxData;
    localSaveStorage(data);
    await HistoryDAO.newMinha(userId, data);
    clearStorageOnBrowser();
    window.location.href = 'perfil.html';
}

function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj)); 
}   


