//Variaveis Globais
var idNpc = 0;
var n_npc = 0;
var data;
let userId;

//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


/*      loadPage()
*       Carrega os dados passados pelos usuario da primeira pagina de criação de história
*/
function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");  
}
 
/*      addRaca()
*       Add o card de raça no html para receber o nome da raça.
*/
function add(){
    //caso esteja setado com a mensagem de alerta a div id=racaMessage volta a ser vazio clicarem no botão add
    document.getElementById('npcMessage').innerHTML = "";

    idNpc++;
    n_npc++;    
    
    var item = `
    
    <div class="card" id="npc_`+idNpc+`">
        <div class="card-header" > 
            <button class="btn btn-sm col-xl-11" data-toggle="collapse" data-target="#collapse`+idNpc+`" aria-expanded="true" aria-controls="collapse`+idNpc+`">
                <input id="nome_`+idNpc+`" name="nomeNPC" class="form-control form-control-sm shadow" type="text" placeholder="Nome do NPC" >
                <div id="_nome_`+idNpc+`" class="text-danger text-left"></div>                
            </button>
            <i onclick="deleteAtributo(npc_`+idNpc+`)" class="far fa-trash-alt ml-3 fa-lg" ></i>            
        </div>     
    
        <div id="collapse`+idNpc+`" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body row">
                <div class="input-group mb-3 mr-3">                        
                    <label class="form-check-label mr-2 ml-1" for="">
                        Esse NPC é: 
                        <span class="" type="button" data-toggle="collapse" data-target="#unicidadeExplicacao_`+idNpc+`" aria-expanded="false" aria-controls="unicidadeExplicacao">
                            <i class="fa fa-question-circle text-warning" aria-hidden="true"></i>
                        </span>
                    </label>     
                    <div class="row">
                        <div class="col text-left mb-2 mt-2">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="unicicade_`+idNpc+`" id="unico_`+idNpc+`" value="unico" checked>
                                <label class="form-check-label" for="inlineRadio1">Único</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="unicicade_`+idNpc+`" id="multiplo_`+idNpc+`" value="multiplo">
                                <label class="form-check-label" for="inlineRadio2">Multiplo</label>
                            </div>

                            <div id="unicidadeMessage`+idNpc+`" class="text-danger text-left"></div>
                        </div>
                    </div>

                    <div class="card mb-1 collapse multi-collapse text-left" id="unicidadeExplicacao_`+idNpc+`">
                        <div class="p-3 " >                                
                                NPC único é:<br>
                                <span style="font-size: 10.5px;">
                                    - um NPC que normalmente tem papel importante na partida. <br>
                                    Ex: O rei, A princesa, O soldado.(Nesse caso, você deve selecionar o campo único).<br>
                                </span><br>
                                NPC multiplo é:<br>
                                <span style="font-size: 10.5px;">
                                    - um NPC que pode ser replicado várias vezes. <br>
                                    Ex: Os lobos, Os soldados, Os orcs(pode ser que em uma determinada cena da história tenham 10 lobos, nesse caso, você deve selecionar o campo multiplo, pois assim poderá adicionar uma quantidade específica desse NPC à cena).<br>
                                </span>
                        </div>
                    </div>
                </div>  

                <div class="col">                    

                    <div class="input-group mb-3 mr-3">
                        <div class="input-group-prepend input-group-sm">
                            <span class="input-group-text" >Raça:</span>
                        </div>
                        
                        <select id="raca_`+idNpc+`" class="form-control form-control-sm shadow">
                                                                                                     
                        </select>
                        <div id="classeMessage`+idNpc+`" class="text-danger text-left"></div>
                    </div>  

                    <div class="input-group mb-3 mr-3">
                        <div class="input-group-prepend input-group-sm">
                            <span class="input-group-text" >Classe:</span>
                        </div>
                        
                        <select id="classe_`+idNpc+`" class="form-control form-control-sm shadow">
                                                                                                     
                        </select>
                        <div id="classeMessage`+idNpc+`" class="text-danger text-left"></div>
                    </div>     

                    <div id="atributos_`+idNpc+`">
                        
                    </div>           
                    
                    
                </div>
                <div class="col">
                        <div class="input-group mb-3 mr-3">
                            <div class="input-group-prepend input-group-sm">
                                <span class="input-group-text h-100" >Descrição:</span>
                            </div>                            
                            
                            <textarea class="form-control " id="descricao_`+idNpc+`" rows="11" placeholder="Insira a descrição dos NPC's se considerar necessário!"></textarea>
                        </div>     
                                        
                </div>
            </div>        
        </div>
    </div>    
    `
    
    document.getElementById("item").insertAdjacentHTML('beforeend', item);  

    // ESSE TRECHO CARREGA AS CLASSES ANTERIORMENTE CRIADAS
    for(x=0; x < JSON.parse(data).classes.length; x++){
        var option = document.createElement('option');        
        option.id = JSON.parse(data).classes[x] + "_" + idNpc; //sera salvo no id ex: Guerreiro_1 
        option.append(JSON.parse(data).classes[x]);        
        document.getElementById('classe_'+idNpc).append(option);        
    }


    // ESSE TRECHO CARREGA AS RAÇAS ANTERIORMENTE CRIADAS
    for(x=0; x < JSON.parse(data).racas.length; x++){
        var option = document.createElement('option');        
        option.id = JSON.parse(data).racas[x] + "_" + idNpc; //sera salvo no id ex: Humano_1 
        option.append(JSON.parse(data).racas[x]);        
        document.getElementById('raca_'+idNpc).append(option);        
    }

    // ESSE TRECHO CARREGA OS ATRIBUTOS ANTERIORMENTE CRIADOS    
    for(x=0; x < JSON.parse(data).atributo_fixo.length; x++){
        var atributo = 
        `
            <div class="input-group mb-3 mr-3 ">
                <div class="input-group-prepend input-group-sm col-md-4">
                    <span class="input-group-text" >`+JSON.parse(data).atributo_fixo[x].nome+`:</span>
                </div>
                                
                    <input id="`+JSON.parse(data).atributo_fixo[x].nome+`_`+idNpc+`" class="form-control form-control-sm shadow" type="number" placeholder="valor máximo: `+JSON.parse(data).atributo_fixo[x].max+`" max="`+JSON.parse(data).atributo_fixo[x].max+`" >
                <div id="_`+JSON.parse(data).atributo_fixo[x].nome+`_`+idNpc+`" class="text-danger text-left"></div>
            </div>
         
        `

        document.getElementById('atributos_'+idNpc).insertAdjacentHTML('beforeend', atributo);  
    }
    //console.log(itemRaca);
}


/*      deleteRaca(id)
*  Entrada: id do div mais externo do card de raça
*  Saida: Remove do código html o card referente ao id passado
*/
function deleteAtributo(id){
    id.remove();
    n_npc--;
}

/*      getId(string)
*  Entrada: uma string, que o id de um dos inputs da página, id="STRING_NUMERO" ex id="max_1"
*  Saida: retorna o número presente na string, número que de acordo com a formação inicial estara na ultima posição da string
*/
function getId(string){
    var retorno = string.split("_");
    return retorno[retorno.length-1];
}

/*      next()
*   Funcao: Ao clicarem no botão "Avançar" a função realiza a verificação se há algum elemento adicionado
*           caso não tenha, exibe a mensagem de alerta "Adicione pelo menos 1 NPC"
*           caso tenha, verifica se o usuário entrou algum valor para os campos de NPC
*               caso não, exibe a mensagem campo obrigatório
*               caso sim, então estamos prontos para criar o JSON
*/
function next(){    
    //alert(document.getElementsByTagName('input').length + document.getElementsByTagName('select').length);
    
    var validador = 0;
    if(document.getElementsByTagName('input').length == 0){
        document.getElementById('npcMessage').innerHTML = "Adicione pelo menos 1 NPC!";
        validador++;
    }else{        
        
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            //alert(document.getElementsByTagName('input').length);
            if(document.getElementsByTagName('input')[x].type != "radio"){
                if(document.getElementsByTagName('input')[x].value == ""){ 
                    var id = document.getElementsByTagName('input')[x].id;
                    document.getElementById("_"+id).innerHTML = "Campo Obrigatório!";                
                    validador++;
                }else{
                    var id = document.getElementsByTagName('input')[x].id;
                    document.getElementById("_"+id).innerHTML = "";                
                }
            }
            
        }    
    }
    
    if(validador == 0){
        var vetor = [];
        //passo por cada npc criado
        for(x=0; x < n_npc; x++){            
            //pego o id pelo campo de nome, cujo o id é o único para cada NPC
            //alert(document.getElementsByName("nome").length);
            var id = getId(document.getElementsByName("nomeNPC")[x].id);                 
            
            var vetor_atributos = [];
            //salvo cada atributo no vetor de atributos
            for(y=0; y < JSON.parse(data).atributo_fixo.length; y++){
                vetor_atributos.push(
                    parseInt(document.getElementById(JSON.parse(data).atributo_fixo[y].nome+`_`+id).value)
                );
            }
            //por fim, insiro o objeto JSON no vetor
            vetor.push(
                {
                    nome: document.getElementById('nome_'+id).value,
                    raca: document.getElementById('raca_'+id).value,
                    classe: document.getElementById('classe_'+id).value, 
                    atributos: vetor_atributos,                    
                    descricao: document.getElementById('descricao_'+id).value,
                    unico: document.getElementById('unico_'+id).checked

                }
            );
            
        }
        //tranformo os dados em objeto e adiciono o vetor ao objeto
        data = JSON.parse(data);
        data["npc"] = vetor;
        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaCena.html';
        
        //console.log(data);
        //console.log(JSON.stringify(data));        
    }  
    
}

function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    
}
