function controlForm() {
	const nomeElem = document.getElementById("input_nome");
	const emailElem = document.getElementById("input_email");
	const senhaElem  = document.getElementById("input_pass");
	const senhaConfElem =document.getElementById("input_passc");

	document.getElementById("input_passc")
		.addEventListener("keyup", ev => {
			// Número 13 corresponde ao "Enter"
			if (ev.keyCode === 13) {
				ev.preventDefault();

				signUp(nomeElem.value, emailElem.value, senhaElem.value, senhaConfElem.value);
			}
		});

	document.getElementById("btn_cadastrar")
		.addEventListener("click", ev => {
			ev.preventDefault();
			signUp(nomeElem.value, emailElem.value, senhaElem.value, senhaConfElem.value);
		});
}

this.signUp = async function(nome, email, senha, senhaConf) {

	if(nome.length <= 3){
		alert("Preencha seu nome");
	}else if(email.length <= 8){
		alert("Preencha seu email");
	}else if(senha.length < 8){
		alert("Senha minima 8 caracteres");
	}else if(senha !== senhaConf){
		alert("Senhas nao sao iguais");
	}else{
		try {
			this.UserDAO.signUp.call(this, nome, email, senha, senhaConf)
				.then( _ => {
					firebase.auth().signOut();
					window.location.href = "./Login.html";
			})
				.catch( err => {
					console.error(err);
					alert("Houve um erro: " + err.message);
			});
		}
		catch(err) {
			console.error(err);
			alert("Erro na formatação dos campos");
		}
	}
}
