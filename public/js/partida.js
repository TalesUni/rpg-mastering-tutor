
let idHistoria;
let nomeHistoria;
let fixos;
let idSave;
let idUser;

// Aciona o salvar atributos antes de sair da pagina
window.onunload = async function(){
    await saveAtributes();
};

async function showSave(save) {
    // Inicio grafo
    let canvas = document.getElementById("grafo");

    // recupera estado
    const estado = await MatchDAO.getEstadoGrafo(idUser, idSave);
    const grafo = await Global.main(idHistoria, "canvasGrafo", parseInt(canvas.style.width), null, estado);
    window.addEventListener('verticeClicado', ev => {
        saveAtributes(grafo);
        loadCena(ev.detail.id);
    }, false);
    // Fim grafo

    // Carrega a primeira cena
    loadCena(save.noAtual);

    // Carrega os players do save
    if (save.players != undefined) {
        loadPlayers(save.players);
    } else {
        let div = document.getElementById("list_player");
        let card = document.createElement('div');
        card.className = "card p-3";
        card.textContent = "Este save não contém nenhum player.";
        div.appendChild(card);
    }
}

async function loadCena(idCena) {
    // Pega do banco de dados a cena em questao
    let cena = await HistoryDAO.getCena(idHistoria, idCena);

    // Atualiza o no atual 
    await MatchDAO.changeAtualNPC(idUser, idSave, idCena);

    // Atualiza o titulo da pagina
    let title = document.getElementById("title");
    title.textContent = nomeHistoria + " - " + cena.titulo;

    // Atualiza as instrucoes do mestre
    let instruction = document.getElementById("instruction");
    if ((cena.orientacao != undefined) && (cena.orientacao != "")) {
        instruction.textContent = cena.orientacao;
    } else  {
        instruction.textContent = "Esta cena não possui instruções";
    }

    // Atualiza o que o mestre deve dizer aos jogadores
    let say = document.getElementById("say");
    if ((cena.descricao != undefined) && (cena.descricao != "")){
        say.textContent = cena.descricao;
    } else  {
        say.textContent = "Esta cena não possui diálogos/descrições especificos";
    }

    // Limpa a lista de NPCs
    limpaDiv("list_npc");

    // Repopula a lista de NPCs
    if (cena.npc != undefined) {
        await loadNPCs(cena.npc);
    }  else {
        let list_npc = document.getElementById("list_npc");
        let card = document.createElement('div');
        card.className = "card p-3";
        card.textContent = "Esta cena não contém nenhum NPC.";
        list_npc.appendChild(card);  
    }

    // Se for um no final, mostra o botao de avaliacao
    let aval = document.getElementById("avaliar");
    if (cena.proximo == undefined) {
        aval.className = "btn bg-orange text-light p-3";
    } else {
        aval.className = "btn bg-orange text-light p-3 d-none";
    }
}

/*  limpaDiv(divId)
*   Entrada: Id da div que deseja excluir todos os filhos
*   Saida: -
*/
function limpaDiv(divId) {
    let div = document.getElementById(divId);
    while (div.children.length != 0){
        div.children[0].remove();
    }
}

function loadPlayers(players) {
    let i=0;
    let div = document.getElementById("list_player");

    for (let n in players) {
        i++;
        let card = document.createElement('div');
        card.className = "card";

        let cardHeader = document.createElement('div');
        cardHeader.className = "card-header";
        cardHeader.id = "hplayer"+i;

        let h4 = document.createElement('h2');
        h4.className = "mb-0";

        let btnCollapse = document.createElement('button');
        btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none d-flex";
        btnCollapse.setAttribute("data-toggle","collapse");
        btnCollapse.setAttribute("data-target","#player"+i);
        btnCollapse.setAttribute("aria-expanded","false");
        btnCollapse.setAttribute("aria-controls","player"+i);
        let color =  document.createElement("input");
        color.setAttribute("type", "color");
        color.setAttribute("value", players[n].cor);
        color.setAttribute("disabled","true");
        color.setAttribute("style","width: 25px;border: none;");

        btnCollapse.appendChild(color);
        
        let pName = document.createElement("a");
        pName.textContent = players[n].nome;
        pName.className = "ml-3 text-decoration-none";
        btnCollapse.appendChild(pName);
        //btnCollapse.textContent = players[n].nome;

        let descr = document.createElement('div');
        descr.id = "player"+i;
        descr.className = "collapse";
        descr.setAttribute("aria-labelledby","hplayer"+i);
        descr.setAttribute("data-parent","#list_player");

        let descrBody = document.createElement('div');
        descrBody.className = "card-body";

        let detalhes = document.createElement('div');
        detalhes.className = "mb-2";

        let classeraca = document.createElement('p');
        classeraca.textContent = players[n].raca +", "+ players[n].classe;
        classeraca.className = "font-italic mb-3";
        
        let atributos = document.createElement('div');
        loadAtributos(atributos, players[n].atributos, n, true);

        let anotacoes = document.createElement('div');
        let anot_title = document.createElement('p');
        anot_title.textContent = "Anotacoes: ";
        let anot_box = document.createElement('textarea');
        anot_box.className = "saveChangeAnot";
        anot_box.id = n + '__Anot';
        anot_box.value = players[n].anotacoes;
        anotacoes.appendChild(anot_title);
        anotacoes.appendChild(anot_box);

        detalhes.appendChild(classeraca);
        detalhes.appendChild(atributos);
        detalhes.appendChild(anotacoes);
        descrBody.appendChild(detalhes);

        // Add card to de page
        h4.appendChild(btnCollapse);
        cardHeader.appendChild(h4);
        descr.appendChild(descrBody);
        card.appendChild(cardHeader);
        card.appendChild(descr);

        // Add card to de page
        div.appendChild(card);
    }
}

async function loadNPCs(npcs) {
    let i=0;
    let div = document.getElementById("list_npc");
        
    for (let n in npcs) {
        let npc = await MatchDAO.getNPC(idUser, idSave, npcs[n].tipo);
        i++;
        let card = document.createElement('div');
        card.className = "card";

        let cardHeader = document.createElement('div');
        cardHeader.className = "card-header";
        cardHeader.id = "heading"+i;

        let h4 = document.createElement('h2');
        h4.className = "mb-0";

        let btnCollapse = document.createElement('button');
        btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none";
        btnCollapse.setAttribute("data-toggle","collapse");
        btnCollapse.setAttribute("data-target","#collapse"+i);
        btnCollapse.setAttribute("aria-expanded","false");
        btnCollapse.setAttribute("aria-controls","collapse"+i);
        btnCollapse.textContent = npc.nome;

        let descr = document.createElement('div');
        descr.id = "collapse"+i;
        descr.className = "collapse";
        descr.setAttribute("aria-labelledby","heading"+i);
        descr.setAttribute("data-parent","#list_npc");

        let descrBody = document.createElement('div');
        descrBody.className = "card-body";
        let detalhes = document.createElement('div');
        detalhes.className = "font-italic";
        detalhes.textContent = npc.raca +", "+ npc.classe + ". " + npc.descricao;
        let atributos = document.createElement('div');
        loadAtributos(atributos, npc.atributos, npcs[n].tipo, npc.unico);
        descrBody.appendChild(detalhes);
        descrBody.appendChild(atributos);

        // Add card to de page
        h4.appendChild(btnCollapse);
        cardHeader.appendChild(h4);
        descr.appendChild(descrBody);
        card.appendChild(cardHeader);
        card.appendChild(descr);

        // Add card to de page
        div.appendChild(card);
    }
}

function loadAtributos(div, atributos, id, unico) {
    for (let a in atributos) {
        let inpuGroup = document.createElement("div");
        inpuGroup.className = "input-group mb-3 mr-3 d-flex";

        let inputPrepend = document.createElement("div");
        inpuGroup.className = "input-group-prepend";

        let span = document.createElement("span");
        span.className = "input-group-text";
        span.textContent = fixos[a].nome;

        let input = document.createElement("input");
        input.setAttribute("type","number");
        input.setAttribute("placeholder","Valor");
        input.setAttribute("aria-label","Valor");
        input.setAttribute("aria-describedby","basic-addon1");
        input.className = "form-control saveChange";
        input.id = id + '__' + a;
        input.value = atributos[a];

        if (!unico) {
            input.className = "form-control";
        } else {
            input.className = "form-control saveChange";
        }

        inputPrepend.appendChild(span);
        inpuGroup.appendChild(inputPrepend);
        inpuGroup.appendChild(input);
        div.appendChild(inpuGroup);
    }
}

async function saveAtributes(grafo = null) {
    // PLAYERS
    let players = document.getElementById("list_player");
    let atributos = players.getElementsByClassName("saveChange");
    for (let i=0; i<atributos.length; i++) {
        let ids = atributos[i].id.split('__');
        let idPlayer = ids[0];
        let idAtr = ids[1];
        await MatchDAO.changeAtributePlayer(idUser, idSave, idPlayer, idAtr, parseInt(atributos[i].value));
    }
    let anotacoes = players.getElementsByClassName("saveChangeAnot");
    for (let i=0; i<anotacoes.length; i++) {
        let idPlayer = anotacoes[i].id.split('__')[0];
        await MatchDAO.changeNotePlayer(idUser, idSave, idPlayer, anotacoes[i].value);
    }

    // NPCS
    let npcs = document.getElementById("list_npc");
    atributos = npcs.getElementsByClassName("saveChange");
    for (let i=0; i<atributos.length; i++) {
        let ids = atributos[i].id.split('__');
        let idNPC = ids[0];
        let idAtr = ids[1];
        await MatchDAO.changeAtributeNPC(idUser, idSave, idNPC, idAtr, parseInt(atributos[i].value));
    }

    //GRAFO
    if(grafo !== null){
        const estado = grafo.getEstado();
        await MatchDAO.changeEstadoGrafo(idUser, idSave, estado);
    }
}

function avaliar() {
    window.location.href = "./avaliar.html?" + idHistoria + "?" + idSave;
}

firebase.auth().onAuthStateChanged(async function(user) {
    if (user) {
        let res = window.location.href.split('?'); 
        if (res[2] != undefined) {
            idHistoria = res[1];
            idSave = res[2];
            idUser = user.uid;
            nomeHistoria = await HistoryDAO.getName(idHistoria);
            fixos = await HistoryDAO.getFixedAtributes(idHistoria);
            let save = await MatchDAO.getSaveById(idUser, idSave);
            showSave(save);
        } else {
            window.location.href = "./perfil.html";
        }
    } else {
        window.location.href = "./index.html";
    }
});