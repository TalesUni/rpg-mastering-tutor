
let idHistoria;
let nomeHistoria;
let fixos;
let idUser;

function show() {
    // Inicio grafo
    let canvas = document.getElementById("grafo");
    Global.main(idHistoria, "canvasGrafo", parseInt(canvas.style.width));
    window.addEventListener('verticeClicado', ev => {
        loadCena(ev.detail.id);
    }, false);
    // Fim grafo

    // Carrega a primeira cena
    loadCena(0);
}

async function loadCena(idCena) {
    // Pega do banco de dados a cena em questao
    let cena = await HistoryDAO.getCena(idHistoria, idCena);

    // Atualiza o titulo da pagina
    let title = document.getElementById("title");
    title.textContent = nomeHistoria + " - " + cena.titulo;

    // Atualiza as instrucoes do mestre
    let instruction = document.getElementById("instruction");
    if ((cena.orientacao != undefined) && (cena.orientacao != "")) {
        instruction.textContent = cena.orientacao;
    } else  {
        instruction.textContent = "Esta cena não possui instruções";
    }

    // Atualiza o que o mestre deve dizer aos jogadores
    let say = document.getElementById("say");
    if ((cena.descricao != undefined) && (cena.descricao != "")){
        say.textContent = cena.descricao;
    } else  {
        say.textContent = "Esta cena não possui diálogos/descrições especificos";
    }

    // Limpa a lista de NPCs
    limpaDiv("list_npc");

    // Repopula a lista de NPCs
    if (cena.npc != undefined) {
        await loadNPCs(cena.npc);
    }  else {
        let list_npc = document.getElementById("list_npc");
        let card = document.createElement('div');
        card.className = "card p-3";
        card.textContent = "Esta cena não contém nenhum NPC.";
        list_npc.appendChild(card);  
    }
}

/*  limpaDiv(divId)
*   Entrada: Id da div que deseja excluir todos os filhos
*   Saida: -
*/
function limpaDiv(divId) {
    let div = document.getElementById(divId);
    while (div.children.length != 0){
        div.children[0].remove();
    }
}

async function loadNPCs(npcs) {
    let i=0;
    let div = document.getElementById("list_npc");
        
    for (let n in npcs) {
        let npc = await HistoryDAO.getNPC(idHistoria, npcs[n].tipo);
        i++;
        let card = document.createElement('div');
        card.className = "card";

        let cardHeader = document.createElement('div');
        cardHeader.className = "card-header";
        cardHeader.id = "heading"+i;

        let h4 = document.createElement('h2');
        h4.className = "mb-0";

        let btnCollapse = document.createElement('button');
        btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none";
        btnCollapse.setAttribute("data-toggle","collapse");
        btnCollapse.setAttribute("data-target","#collapse"+i);
        btnCollapse.setAttribute("aria-expanded","false");
        btnCollapse.setAttribute("aria-controls","collapse"+i);
        btnCollapse.textContent = npc.nome;

        let descr = document.createElement('div');
        descr.id = "collapse"+i;
        descr.className = "collapse";
        descr.setAttribute("aria-labelledby","heading"+i);
        descr.setAttribute("data-parent","#list_npc");

        let descrBody = document.createElement('div');
        descrBody.className = "card-body";
        let detalhes = document.createElement('div');
        detalhes.className = "font-italic";
        detalhes.textContent = npc.descricao;
        let atributos = document.createElement('div');
        loadAtributos(atributos, npc.atributos);
        descrBody.appendChild(detalhes);
        descrBody.appendChild(atributos);

        // Add card to de page
        h4.appendChild(btnCollapse);
        cardHeader.appendChild(h4);
        descr.appendChild(descrBody);
        card.appendChild(cardHeader);
        card.appendChild(descr);

        // Add card to de page
        div.appendChild(card);
    }
}

function loadAtributos(div, atributos) {
    for (let a in atributos) {
        let titulos = document.createElement('div');
        titulos.className = "row";

        let p = document.createElement('p');
        p.className = "col col-sm-7 text-center text-sm-left";
        p.textContent = fixos[a].nome ;
        titulos.appendChild(p);
        let d = document.createElement('input');
        d.setAttribute("disabled", "true");
        d.className = "col col-sm-4 text-center text-sm-left ";
        d.value =  atributos[a];
        titulos.appendChild(d);

        div.appendChild(titulos);
    }
}

async function newSave() {
    let idHistoria = window.location.href.split('?')[1];
    window.location.href = './inicioPartida.html?'+idHistoria;
}

firebase.auth().onAuthStateChanged(async function(user) {
    if (user) {
        let res = window.location.href.split('?'); 
        if (res[1] != undefined) {
            idHistoria = res[1];
            idUser = user.uid;
            nomeHistoria = await HistoryDAO.getName(idHistoria);
            fixos = await HistoryDAO.getFixedAtributes(idHistoria);
            show();
        } else {
            window.location.href = "./biblioteca.html";
        }
    } else {
        window.location.href = "./index.html";
    }
});