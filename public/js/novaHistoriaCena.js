//Variaveis Globais
var idCena = 0;
var n_cena = 0;
var data;
let userId;

//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


/*      loadPage()
*       Carrega os dados passados pelos usuario da primeira pagina de criação de história
*/
function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");
}
 
/*      add()
*       Add o card no html.
*/
function add(){
    //caso esteja setado com a mensagem de alerta a div id=racaMessage volta a ser vazio clicarem no botão add
    document.getElementById('cenaMessage').innerHTML = "";

    idCena++;
    n_cena++;    
    
    var item = `
    
    <div class="card" id="cena_`+idCena+`">
        <div class="card-header" > 
            <button class="btn btn-sm col-xl-9" data-toggle="collapse" data-target="#collapse`+idCena+`" aria-expanded="true" aria-controls="collapse`+idCena+`">
                <input id="nome_`+idCena+`" name="titulo" class="form-control form-control-sm shadow" type="text" placeholder="Título da Cena" >
                <div id="_nome_`+idCena+`" class="text-danger text-left"></div>                
            </button>
            
            <input class="form-check-input align-middle" name="radio" type="radio" name="primeiraCena" id="primeiraCena_`+idCena+`">
            <label class="form-check-label align-middle" for="flexRadioDefault1">
                Primeira Cena
            </label>

            <i onclick="deleteAtributo(cena_`+idCena+`)" class="far fa-trash-alt ml-3 fa-lg align-middle" ></i>            
        </div>     
    
        <div id="collapse`+idCena+`" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body row"> 
                <div class="col">
                    <div class="input-group mb-3 mr-3">
                        <div class="input-group-prepend input-group-sm">
                            <span class="input-group-text h-100" >Descrição:</span>
                        </div>                            
                        
                        <textarea class="form-control " id="descricao_`+idCena+`" rows="5" placeholder="Insira a descriação da Cena!"></textarea>
                    </div>        
                    
                    <div class="input-group mb-3 mr-3">
                        <div class="input-group-prepend input-group-sm">
                            <span class="input-group-text h-100" >Orientação:</span>
                        </div>                            
                        
                        <textarea class="form-control " id="orientacao_`+idCena+`" rows="5" placeholder="Insira uma orientação, essa orientação é para o mestre da história!"></textarea>
                    </div>    

                    <div class="display-block mb-3 text-center">                       
                        <span class="input-group-text h-100 justify-content-center" >NPC's:</span>                      
                        
                        <div class="card-body border text-center" id="npcs_`+idCena+`">

                        </div>
                    </div>    

                </div>
            </div>    
        </div>
    </div>    
    `
    
    document.getElementById("item").insertAdjacentHTML('beforeend', item);  

    // ESSE TRECHO CARREGA OS NPCS ANTERIORMENTE CRIADOS    
    //alert(JSON.parse(data).npc.length);
    for(x=0; x < JSON.parse(data).npc.length; x++){
        //alert(JSON.parse(data).npc[x].unico);
        var npcs;
        var html_especial;
        if(JSON.parse(data).npc[x].unico){
            html_especial = 
            `
            <div class="input-group-prepend text-center">
                 <span class="input-group-text justify-content-center" >NPC - ÚNICO</span>                          
            </div>
            `             
        }else{
            html_especial =
            `
            <div class="input-group">
                <div class="input-group-prepend input-group-sm">
                    <span class="input-group-text" >Quantidade de `+JSON.parse(data).npc[x].nome+`:</span>
                </div>
                <input id="qtd_`+JSON.parse(data).npc[x].nome+`_`+idCena+`"  class="form-control form-control-sm" type="number" min="0" placeholder="0" value="0">
                <div id="quantityMessage_`+JSON.parse(data).npc[x].nome+`_`+idCena+`" class="text-danger text-left"></div>
            </div>
            `
        }
        npcs =   
         `   <div class="row">    
                <div class="col col-md-7 text-center">
                    `+html_especial+`           
                </div>
    
                <div class="col text-left">
                    <input class="form-check-input" type="checkbox" value="`+x+`" id="npc_`+JSON.parse(data).npc[x].nome+`_`+idCena+`" name="npc_`+idCena+`">
                    <label class="form-check-label mr-2 ml-1" for="`+x+`">
                        `+JSON.parse(data).npc[x].nome+`
                    </label>         
                </div>
                <hr class="mb-2 mt-2">
            </div>   
         
        `
        
        document.getElementById('npcs_'+idCena).insertAdjacentHTML('beforeend', npcs);  
    }    
}


/*      deleteRaca(id)
*  Entrada: id do div mais externo do card de raça
*  Saida: Remove do código html o card referente ao id passado
*/
function deleteAtributo(id){
    id.remove();
    n_cena--;
}

/*      getId(string)
*  Entrada: uma string, que o id de um dos inputs da página, id="STRING_NUMERO" ex id="max_1"
*  Saida: retorna o número presente na string, número que de acordo com a formação inicial estara na ultima posição da string
*/
function getId(string){
    var retorno = string.split("_");
    return retorno[retorno.length-1];
}

/*      next()
*   Funcao: Ao clicarem no botão "Avançar" a função realiza a verificação se há algum elemento adicionado
*           caso não tenha, exibe a mensagem de alerta "Adicione pelo menos 1 NPC"
*           caso tenha, verifica se o usuário entrou algum valor para os campos de NPC
*               caso não, exibe a mensagem campo obrigatório
*               caso sim, então estamos prontos para criar o JSON
*/
function next(){    
    //alert(document.getElementsByTagName('input').length + document.getElementsByTagName('select').length);
    
    var validador = 0;
    let check_radio = false;
    if(document.getElementsByTagName('input').length == 0){
        document.getElementById('cenaMessage').innerHTML = "Adicione pelo menos 1 Cena!";
        validador++;
    }else{        
        
        //Nesse for eu verifico cada Cena teve o nome entrado.
        for(x=0; x < document.getElementsByName('titulo').length; x++){
            //Esse primeiro bloco if then else percorre todos os título e verifica se estão vazios ou não;
            if(document.getElementsByName('titulo')[x].value == ""){ 
                var id = document.getElementsByName('titulo')[x].id;
                document.getElementById("_"+id).innerHTML = "Campo Obrigatório!";                
                validador++;
            }else{
                var id = document.getElementsByName('titulo')[x].id;
                document.getElementById("_"+id).innerHTML = "";                
            }            

            //Esse segundo bloco if then else percorre todos os input radio e verifica se o criador setou a primeira cena
            if(document.getElementsByName('radio')[x].checked == true){                
                check_radio = true;
            }

            //Esse for percorre todos os NPC verificando NPCs não unicos, os que forem multiplos precisam de uma quantidade maior que zero
            for(i in JSON.parse(data).npc){
                let id = getId(document.getElementsByName('titulo')[x].id);  
                if(document.getElementById('npc_'+JSON.parse(data).npc[i].nome+'_'+id).checked == true && JSON.parse(data).npc[i].unico == false){                    
                    let value = document.getElementById('qtd_'+JSON.parse(data).npc[i].nome+'_'+id).value;
                    if(parseInt(value) <= 0 || value.toString() == ""){
                        document.getElementById('quantityMessage_'+JSON.parse(data).npc[i].nome+'_'+id).innerHTML = "Valor Inválido!"
                        validador++;
                    }else{
                        //console.log(document.getElementById('qtd_'+JSON.parse(data).npc[i].nome+'_'+id).value);
                        document.getElementById('quantityMessage_'+JSON.parse(data).npc[i].nome+'_'+id).innerHTML = "";
                        
                    }                            
                }else if(document.getElementById('npc_'+JSON.parse(data).npc[i].nome+'_'+id).checked == false && JSON.parse(data).npc[i].unico == false){
                    document.getElementById('quantityMessage_'+JSON.parse(data).npc[i].nome+'_'+id).innerHTML = "";
                    //console.log(document.getElementById('qtd_'+JSON.parse(data).npc[i].nome+'_'+id).value);
                }
            }     

        }
        

        //Se dentro do for a condição der true em algum momento, então a flag check_radio é setada como true e a mensagem não é exibida
        if(check_radio==false){
            document.getElementById('radioMessage').innerHTML = "Selecione a Primeira Cena de sua história!";
        }else{
            document.getElementById('radioMessage').innerHTML = "";
        }


    }
    
    
    if(validador == 0 && check_radio==true){
        var vetor = [];
        var json_obj;   
        
        //passo por cada cena criada
        for(x=0; x < n_cena; x++){            
            //pego o id pelo getElementsByName é o único para cada Título de Cena
            var id = getId(document.getElementsByName('titulo')[x].id);  
            console.log(id);
            /*  percorro os npcs para verificar foram adicionados a cena, 
                se sim, verifico se são unicos, 
                    se sim então adiciono 1
                    senão verifico a quantidade e adiciono a quantidade desejada */
           var vetor_npc = [];
            for(i in JSON.parse(data).npc){
                
                if(document.getElementById('npc_'+JSON.parse(data).npc[i].nome+'_'+id).checked == true){
                    //console.log("NPC: "+JSON.parse(data).npc[i].nome + " Status: "+ document.getElementById('npc_'+JSON.parse(data).npc[i].nome+'_'+id).checked);                
                    //console.log("PRIMEIRA CENA: "+document.getElementById("primeiraCena_"+id).checked);
                    if(JSON.parse(data).npc[i].unico == true){
                        //console.log("Unico");
                        vetor_npc.push(
                            {
                                tipo: parseInt(i),
            
                            }
                        );

                    }else{
                        console.log(parseInt(document.getElementById('qtd_'+JSON.parse(data).npc[i].nome+'_'+id).value));
                        for(qtd=0; qtd < parseInt(document.getElementById('qtd_'+JSON.parse(data).npc[i].nome+'_'+id).value); qtd++){
                            vetor_npc.push(
                                {
                                    tipo: parseInt(i),
                                                    
                                }
                            );
                        }
                    }                
                }                
            }
            //console.log(vetor_npc);         


            if(document.getElementById("primeiraCena_"+id).checked==true){
                vetor.unshift(
                    {
                        titulo: document.getElementById('nome_'+id).value,
                        descricao: document.getElementById('descricao_'+id).value,
                        orientacao: document.getElementById('orientacao_'+id).value,
                        npc: vetor_npc,
                        proximo: [],
                        altura: 0
                    }
                );
            }else{
                vetor.push(
                    {
                        titulo: document.getElementById('nome_'+id).value,
                        descricao: document.getElementById('descricao_'+id).value,
                        orientacao: document.getElementById('orientacao_'+id).value,
                        npc: vetor_npc,
                        proximo: [],
                        altura: undefined
                    }
                );

            }
        }   
        
        //tranformo os dados em objeto e adiciono o vetor ao objeto
        data = JSON.parse(data);
        data["cenas"] = vetor;
        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaConexaoCenas.html';
        
        //console.log(data);
        //console.log(JSON.stringify(data));      
    }
    
        
}  
    


function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    
}
