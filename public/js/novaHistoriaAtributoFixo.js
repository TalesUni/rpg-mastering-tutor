let userId;
//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


//Variaveis Globais
var idAtributo = 0;
var n_atributo = 0;
var data;

/*      loadPage()
*       Carrega os dados passados pelos usuario da primeira pagina de criação de história
*/
function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");  
}
 
/*      addRaca()
*       Add o card de raça no html para receber o nome da raça.
*/
function addRaca(){
    //caso esteja setado com a mensagem de alerta a div id=racaMessage volta a ser vazio clicarem no botão add
    document.getElementById('atributoMessage').innerHTML = "";

    idAtributo++;
    n_atributo++;
    
    var item = `
    
    <div class="card-header" id="atributo_`+idAtributo+`">                            
        <button class="btn btn-sm col-xl-11" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <div class="row">
                <div class="col col-md-5">
                    <input id="nome_`+idAtributo+`" class="form-control form-control-sm shadow" type="text" placeholder="Nome do Atributo" >
                    <div id="nomeMessage`+idAtributo+`" class="text-danger text-left"></div>
                </div>
                <div class="col col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend input-group-sm">
                            <span class="input-group-text" >Valor Máximo:</span>
                        </div>
                        <input id="max_`+idAtributo+`" class="form-control form-control-sm shadow" type="number" placeholder="" >
                        <div id="maxMessage`+idAtributo+`" class="text-danger text-left"></div>
                    </div>
                </div>
                <div class="col col-md-1">
                    <i onclick="deleteAtributo(atributo_`+idAtributo+`)" class="far fa-trash-alt fa-lg" ></i>
                </div>
                
            </div>
            
        </button>    
    </div>                                
    
    `
    document.getElementById("atributos").insertAdjacentHTML('beforeend', item);  
    //console.log(itemRaca);
}


/*      deleteRaca(id)
*  Entrada: id do div mais externo do card de raça
*  Saida: Remove do código html o card referente ao id passado
*/
function deleteAtributo(id){
    id.remove();
    n_atributo--;
}

/*      getId(string)
*  Entrada: uma string, que o id de um dos inputs da página, id="STRING_NUMERO" ex id="max_1"
*  Saida: retorna o número presente na string, número que de acordo com a formação inicial estara na ultima posição da string
*/
function getId(string){
    var retorno = string.split("_");
    return retorno[retorno.length-1];
}

/*      next()
*   Funcao: Ao clicarem no botão "Avançar" a função realiza a verificação se há algum elemento adicionado
*           caso não tenha, exibe a mensagem de alerta "Adicione pelo menos 1 raça"
*           caso tenha, verifica se o usuário entrou algum valor para o nome da raça
*               caso não, exibe a mensagem campo obrigatório
*               caso sim, então estamos prontos para criar o JSON
*/
function next(){    
    var validador = 0;
    if(document.getElementsByTagName('input').length == 0){
        document.getElementById('atributoMessage').innerHTML = "Adicione pelo menos 1 atributo fixo!";
        validador++;
    }else{
        
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            if(document.getElementsByTagName('input')[x].value == ""){
                var id = getId(document.getElementsByTagName('input')[x].id);                
                if(document.getElementsByTagName('input')[x].type == "text"){
                    document.getElementById('nomeMessage'+id).innerHTML = "Campo Obrigatório!";
                }else if(document.getElementsByTagName('input')[x].type == "number"){
                    document.getElementById('maxMessage'+id).innerHTML = "Campo Obrigatório!";
                }
                
                validador++;
            }else{
                var id = getId(document.getElementsByTagName('input')[x].id);                
                if(document.getElementsByTagName('input')[x].type == "text"){
                    document.getElementById('nomeMessage'+id).innerHTML = "";
                }else if(document.getElementsByTagName('input')[x].type == "number"){
                    document.getElementById('maxMessage'+id).innerHTML = "";
                }
                //var id = getId(document.getElementsByTagName('input')[x].id);                
                //document.getElementById('atributoMessage'+id).innerHTML = "";
            }
        }        
    }
    
    if(validador == 0){
        var vetor = [];
        //adiciono as racas a um vetor de String
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            if(document.getElementsByTagName('input')[x].type == "text"){
                //o id é um numero que é o mesmo para o atributo Nome e atributo valor max                
                var id = getId(document.getElementsByTagName('input')[x].id);                
                
                vetor.push(
                    {
                        nome: document.getElementById('nome_'+id).value,
                        max: parseInt(document.getElementById('max_'+id).value)
                    }
                );
            }
        }

        //tranformo os dados em objeto e adiciono o vetor ao objeto
        data = JSON.parse(data);
        data["atributo_fixo"] = vetor;
        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaNPC.html';

        //console.log(data);
        //console.log(JSON.stringify(data));        
    }
    
}

function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    
}
