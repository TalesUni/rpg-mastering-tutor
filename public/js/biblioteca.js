
async function loadPage(){
    showUniverses(await HistoryDAO.getAllUniverses());
    showRPGs(await HistoryDAO.getAll());
}

function checkFilters(){
    // Get the page URL
    var res = getUrl();
    var checked = [];

    //If there is parameters, check if it is for search or filter and save them
    if (res[1] != undefined) {
        var type = res[1].split('=');

        if (type[0] == "filter") {
            var parameters = type[1].split('&');

            // Save witch Universe filter option is checked
            for (let i = 0; i<parameters.length; i++) {
                var filter = parameters[i].split('_');

                if (filter[0] == "uni") {
                    for (let j=1; j<filter.length; j++){
                        checked.push(filter[j]);
                    }
                    break;
                } else {
                    for (let j=1; j<filter.length; j++) {
                        var par = document.getElementById(filter[0]+"_"+filter[j]);
                        par.setAttribute("checked", "");
                    }                    
                }
            }
        }
    }
    return checked;
}

function checkFiltersORSearch(){
    // Get the page URL
    var res = getUrl();

    var parameter;
    var parameters;
    //If there is parameters, check if it is for search or filter and save them
    if (res[1] != undefined) {
        var type = res[1].split('=');

        if (type[0] == "search") {
            var parameters = type[1].split('%20');
            var parameter = "";
            for (i=0; i<parameters.length; i++) {
                if (i>0) {
                    parameter += " ";
                }
                parameter += parameters[i];
            }

            // Keep the parameter in the element
            var y = document.getElementById("search");
            y.value = parameter;
            return [parameter, type];
        } else { 
            var parameters = type[1].split('&');
            return [parameters, type];
        }
    }

    return [null, null];

}

function getUrl(){
    return window.location.href.split('?');
}

function showUniverses(universes) {    
    var checked = checkFilters();

    // Get the Universes in the firebase. Each one is going to have an id = i;
    for (let i=1; i<universes.length+1; i++) {
        let data = universes[i-1];

        // Get the div that will contain the Universes
        var div = document.getElementById("universe");

        // Create a new option for the Universe filter
        var filtro = document.createElement('div');
        var input = document.createElement('input');
        var label = document.createElement('label');

        // Add design to the items
        filtro.className = "form-check";
        input.className = "form-check-input";
        input.type = "checkbox";
        input.name = "uni";
        input.value = i;
        input.id = "uni_" + i;
        label.className = "form-check-label";
        label.for = "uni_" + i;
        label.textContent = data.nome;

        // Check if this option was already checked
        for (let j=0; j<checked.length; j++) {
            if (checked[j] == i) {
                input.setAttribute("checked", "");
            }
        }            

        // Add items to each other
        filtro.appendChild(input);
        filtro.appendChild(label);

        // Add Universe filter option to de page
        div.appendChild(filtro);
    }
}

function isThereElement(){
  // If there is no elements in the page at the end
  var elements = document.getElementById("accordion");
  if (elements.childNodes.length == 0) {
      var card = document.createElement('div');

      card.className = "card text-center p-3";
      card.textContent = "Nenhuma historia encontrada";

      elements.appendChild(card);
  }
}

async function showOrNot(data){    
    var [parameters, type] = checkFiltersORSearch();
    
    // Check if the current RPG respects the parameters, if there is one.
    if (parameters != null) {
        if (type[0] == "filter") {
            // If the RPG has any filter
            if (data.filtro != undefined) {
                var j;
                for (j=0; j<parameters.length; j++) {
                    var yparts = parameters[j].split("_");

                    switch (yparts[0]) {
                        case "aval":
                            if (yparts[1] > data.filtro.avaliacao) {
                                return 0;
                            }
                        break;
                        case "qtdjog":
                            if (yparts[1] > data.filtro.qtdjogador) {
                                return 0;
                            }
                        break;
                        case "dific":
                            var k;
                            for (k=1; k<yparts.length; k++) {
                                if (yparts[k] == data.filtro.dificuldade) {
                                    break;
                                }
                            }
                            if (k == yparts.length) {
                                return 0;
                            }
                        break;
                        case "durac":
                            var k;
                            for (k=1; k<yparts.length; k++) {
                                if (yparts[k] == data.filtro.duracao) {
                                    break;
                                }
                            }
                            if (k == yparts.length) {
                                return 0;
                            }
                        break;
                        case "uni":
                            var k;
                            for (k=1; k<yparts.length; k++) {
                                if (yparts[k] == data.filtro.universo) {
                                    break;
                                }
                            }
                            if (k == yparts.length) {
                                return 0;
                            }
                        break;
                    }
                }
            } else {
                return 0;
            }
        } else { // Check if some part of the RPG has the word searched
            let nomeAutor = await UserDAO.getName(data.autor);
            if ((data.nome.toLowerCase().search(parameters.toLowerCase()) == -1) 
            && (data.descricao.toLowerCase().search(parameters.toLowerCase()) == -1) 
            && (nomeAutor.toLowerCase().search(parameters.toLowerCase()) == -1)) {
                return 0;
            }
        }
    }

    return 1;
}

async function showRPGs(rpgs) {
    // Get the values of each RPG in the firebase. Each one is going to have an id = i;

    for (let i=1; i<rpgs.length+1;i++) {
        let data = rpgs[i-1];
        // If the RPG respects the parameters, add it to the page
        if (await showOrNot(data)) {
            // Get the div that will contain the RPGs
            var div = document.getElementById("accordion");

            // Create a new card and its items
            var card = document.createElement('div');
            var cardHeader = document.createElement('div');
            var h4 = document.createElement('h2');
            var btnCollapse = document.createElement('button');
            var descr = document.createElement('div');
            var descrBody = document.createElement('div');
            var author = document.createElement('h6');
            var descrBottom = document.createElement('div');
            var play = document.createElement('button');
            var divPlay = document.createElement('div');

            // Add design to the items
            card.className = "card historia";
            cardHeader.className = "card-header";
            cardHeader.id = "heading"+i;
            h4.className = "mb-0";
            btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100";
            btnCollapse.setAttribute("data-toggle","collapse");
            btnCollapse.setAttribute("data-target","#collapse"+i);
            btnCollapse.setAttribute("aria-expanded","false");
            btnCollapse.setAttribute("aria-controls","collapse"+i);
            btnCollapse.textContent = data.nome;
            descr.id = "collapse"+i;
            descr.className = "collapse";
            descr.setAttribute("aria-labelledby","heading"+i);
            descr.setAttribute("data-parent","#accordion");
            descrBody.className = "card-body";
            descrBody.textContent = data.descricao;
            author.className = "card-body";
            author.textContent = "Autor: " + await UserDAO.getName(data.autor);
            descrBottom.className = "d-flex";
            play.className = "btn bg-orange text-light p-3";
            play.setAttribute("onclick","paginaHistoria('"+data.id+"')");
            play.type = "button";
            play.textContent = "Jogar";
            divPlay.className = "input-group-prepend ml-auto p-3";

            // If there is any filter for the RPG, add it to the card
            var cardFilters = document.createElement('ul');
            cardFilters.className = "list-group list-group-horizontal-xl list-group-horizontal-lg p-3";
            cardFilters.id = "filtro_historia";
            if (data.filtro != undefined) {
                // If there is an Rating Filter, add it to the card
                var cardFilter = document.createElement('li');

                cardFilter.className = "list-group-item h6";
                if (data.filtro.avaliacao != undefined) {
                    cardFilter.textContent = "Nota: " + data.filtro.avaliacao;
                } else {
                    cardFilter.textContent = "Sem Avaliação";
                }
                cardFilters.appendChild(cardFilter);

                // If there is an Level Filter, add it to the card
                if (data.filtro.dificuldade != undefined) {
                    var cardFilter = document.createElement('li');
                    
                    if (data.filtro.dificuldade == "1") {
                        cardFilter.textContent = "Fácil";
                    } else if (data.filtro.dificuldade == "2") {
                        cardFilter.textContent = "Intermediário";
                    } else if (data.filtro.dificuldade == "3") {
                        cardFilter.textContent = "Dificil";
                    } 

                    cardFilter.className = "list-group-item h6";
                    cardFilters.appendChild(cardFilter);
                }

                // If there is an Duration Filter, add it to the card
                if (data.filtro.duracao != undefined) {
                    var cardFilter = document.createElement('li');

                    if (data.filtro.duracao == "1") {
                        cardFilter.textContent = "+/- 30min";
                    } else {
                        cardFilter.textContent = "+/- " + (parseInt(data.filtro.duracao)-1).toString() + "h";
                    }

                    cardFilter.className = "list-group-item h6";
                    cardFilters.appendChild(cardFilter);
                }

                // If there is an Number of Players Filter, add it to the card
                if (data.filtro.qtdjogador != undefined) {
                    var cardFilter = document.createElement('li');

                    cardFilter.className = "list-group-item h6";
                    cardFilter.textContent = (parseInt(data.filtro.qtdjogador)+1).toString() + " ou + jogadores";
                    cardFilters.appendChild(cardFilter);
                }

                // If there is an Universe Filter, add it to the card
                if (data.filtro.universo != undefined) {
                    var cardFilter = document.createElement('li');
                    var universe = document.getElementById("uni_"+ data.filtro.universo);

                    cardFilter.className = "list-group-item h6";
                    cardFilter.textContent = universe.parentNode.childNodes[1].textContent;
                    cardFilters.appendChild(cardFilter);
                }

            } else {
                // If there is no filter in this RPG
                var cardFilter = document.createElement('li');

                cardFilter.className = "list-group-item h6";
                cardFilter.textContent = "Sem tags";
                cardFilters.appendChild(cardFilter);
            }

            // Add items to each other
            h4.appendChild(btnCollapse);
            cardHeader.appendChild(h4);
            card.appendChild(cardHeader);
            divPlay.appendChild(play);
            descrBottom.appendChild(cardFilters);
            descrBottom.appendChild(divPlay);
            descr.appendChild(descrBody);
            descr.appendChild(author);
            descr.appendChild(descrBottom);
            card.appendChild(descr);

            // Add card to de page
            div.appendChild(card);
        }
    }

    // If there is no elements in the page at the end
    isThereElement();
}

function paginaHistoria(x) {
    window.location.href = "./historia.html?" + x;
}

function filter() {
    var i, y, z;

    // Create the parameters for the URL
    var x = "?filter";

    // Get the ranking values
    y = document.getElementsByName("aval");
    for (i=0; i<y.length; i++) {
        if (y[i].checked) {
            x += "=" + y[i].id;
        }
    }

    // Get the number of players values
    y = document.getElementsByName("qtdjog");
    for (i=0; i<y.length; i++) {
        if (y[i].checked) {
            x += "&" + y[i].id;
        }
    }

    // Get the duration values
    y = document.getElementsByName("durac");
    z = "";
    for (i=0; i<y.length; i++) {
        if (y[i].checked) {
            if (z.length == 0) {
                z += "&" + y[i].id;
            } else {
                z += "_" + y[i].value;
            }
        }
    }
    x += z;

    // Get the level values
    y = document.getElementsByName("dific");
    z = "";
    for (i=0; i<y.length; i++) {
        if (y[i].checked) {
            if (z.length == 0) {
                z += "&" + y[i].id;
            } else {
                z += "_" + y[i].value;
            }
        }
    }
    x += z;

    // Get the universe values
    y = document.getElementsByName("uni");
    z = "";
    for (i=0; i<y.length; i++) {
        if (y[i].checked) {
            if (z.length == 0) {
                z += "&" + y[i].id;
            } else {
                z += "_" + y[i].value;
            }
        }
    }
    x += z;

    // Add parameters to the URL
    window.location.href = x;
}

function limpar() {
    // Clean all the parameters in the URL
    window.location.href = "./biblioteca.html";
}

function enterSearch() {
    // If the key pressed was the enter key, search.
    var key = event.keyCode;;
    if (key == 13) {
        search();
    }
}

function search() {
    // Get the word to be searched for
    const y = document.getElementById("search");

    if (y.value != "") {
        // If there is a word there, add the parameter to the URL
        const x = "?search=" + y.value;
        window.location.href = x;
    } else {
        // If there isn't, clean the parameters from the URL
        limpar();
    }
}
