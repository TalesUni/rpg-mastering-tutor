//Variaveis Globais
var idRaca = 0;
var data;
let userId;

//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


/*      loadPage()
*       Carrega os dados passados pelos usuario da primeira pagina de criação de história
*/
function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");  
}
 
/*      addRaca()
*       Add o card de raça no html para receber o nome da raça.
*/
function addRaca(){
    //caso esteja setado com a mensagem de alerta a div id=racaMessage volta a ser vazio clicarem no botão add
    document.getElementById('racaMessage').innerHTML = "";

    idRaca++;
    
    var itemRaca = `
    <div class="card-header" id="raca`+idRaca+`">                            
    <button class="btn btn-sm col-xl-11" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <input id="`+idRaca+`" class="form-control form-control-sm shadow" type="text" placeholder="Nome da Raça" >
        <div id="racaMessage`+idRaca+`" class="text-danger text-left"></div>
    </button>
    <i onclick="deleteRaca(raca`+idRaca+`)" class="far fa-trash-alt ml-3 fa-lg" ></i>
    <div id="racaMessage`+idRaca+`" class="text-danger text-left"></div>
    </div>                                
    
    `
    document.getElementById("racas").insertAdjacentHTML('beforeend', itemRaca);  
    //console.log(itemRaca);
}


/*      deleteRaca(id)
*  Entrada: id do div mais externo do card de raça
*  Saida: Remove do código html o card referente ao id passado
*/
function deleteRaca(id){
    id.remove();
}

/*      next()
*   Funcao: Ao clicarem no botão "Avançar" a função realiza a verificação se há algum elemento adicionado
*           caso não tenha, exibe a mensagem de alerta "Adicione pelo menos 1 raça"
*           caso tenha, verifica se o usuário entrou algum valor para o nome da raça
*               caso não, exibe a mensagem campo obrigatório
*               caso sim, então estamos prontos para criar o JSON
*/
function next(){    
    var validador = 0;
    if(document.getElementsByTagName('input').length == 0){
        document.getElementById('racaMessage').innerHTML = "Adicione pelo menos 1 raça!";
        validador++;
    }else{
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            if(document.getElementsByTagName('input')[x].value == ""){
                var id = document.getElementsByTagName('input')[x].id;
                document.getElementById('racaMessage'+id).innerHTML = "Campo Obrigatório!";
                validador++;
            }else{
                var id = document.getElementsByTagName('input')[x].id;
                document.getElementById('racaMessage'+id).innerHTML = "";
            }
        }
    }

    if(validador == 0){
        var vetor = [];
        //adiciono as racas a um vetor de String
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            vetor.push(document.getElementsByTagName('input')[x].value);
        }

        //tranformo os dados em objeto e adiciono o vetor ao objeto
        data = JSON.parse(data);
        data["racas"] = vetor;
        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaClasse.html';

        //console.log(data);
        //console.log(JSON.stringify(data));
    }    
}

function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    
}
