let userId;
//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});

/*      loadPage()
*  Entrada: nenhuma
*  Saida: Carregando os dados comuns do banco para a página Criação de História    
*/
function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");

    var difficulty = HistoryDAO.getAllDifficulty();
    addOptions(difficulty, "difficulty");
    
    var duration = HistoryDAO.getAllDuration();
    addOptions(duration, "time");    
    
    var universes = HistoryDAO.getAllUniverses();
    addOptions(universes,"universe");

    var quantity = HistoryDAO.getAllQttPlayer();
    addOptions(quantity, "number");


    //setando o formato de saida para mensagem de erro
    for(x=0; x < document.getElementsByName('entrada').length; x++){
        let id = document.getElementsByName('entrada')[x].id;
        warningFormat(document.getElementById(id+'Message'));
    }
}

/*      addOptions()
*  Entrada: obj - um objeto retornado pelo HistoryDao
*           string - uma string que representa um id do html            
*  Saida: Carregando os dados comuns do banco para a página Criação de História    
*/
function addOptions(obj, string){
  var p = Promise.resolve(obj);
    
    p.then(function(vetor) {
      
      var select = document.getElementById(string); 
      for(x=0; x< vetor.length; x++){
        var option = document.createElement('option');
        option.id = x; // atribuo nome universo ao id do elemento option -> <option id="String">
        option.append(vetor[x].nome); // insiro cada nome em um elemento option
        select.append(option);  // insiro cada option dentro do select              
        //console.log(vetor[x].nome);
      }
      
    });
}

/*      next()()
*  Entrada: obj - um objeto retornado pelo HistoryDao
*           string - uma string que representa um id do html            
*  Saida: Verifica se os campos foram preenchidos, caso tenham sido preenchidos cria o Json
*          Apos criar o Json, salva na memoria do Browser e carrega a pagina seguinte
*/
function next(){        
    let validador = 0;    

    //verificando se tá o campo foi devidamente preenchido
    for(x=0; x < document.getElementsByName('entrada').length; x++){
        let id = document.getElementsByName('entrada')[x].id;
        //se os campos quem tem a tag name, não tiverem selecionado a opção de entrada, apresenta a mensagem de erro.
        if(document.getElementById(id).selectedIndex == 0){            
            warningMessageRequiredField(document.getElementById(id+'Message'));
            validador++;
        }else{
            document.getElementById(id+'Message').innerHTML = " ";
        }

    }    

    //---------- se todos os campos forem true, podemos criar o Objeto Json -----------------

    //var selector = document.getElementById('id_of_select');
    //var value = selector[selector.selectedIndex].value;
    if(validador == 0){
        var data = {
            "nome": document.getElementById('name').value,
            "filtro": {                
                "dificuldade": parseInt(document.getElementById('difficulty')[document.getElementById('difficulty').selectedIndex].id)+1,
                "duracao" : parseInt(document.getElementById('time')[document.getElementById('time').selectedIndex].id)+1,
                "qtdjogador" : parseInt(document.getElementById('number')[document.getElementById('number').selectedIndex].id)+1,
                "universo" : parseInt(document.getElementById('universe')[document.getElementById('universe').selectedIndex].id)+1,    
            },
            "descricao": document.getElementById('description').value,
            "regras": document.getElementById('instruction').value

        }        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaRaca.html';    
    }
    //console.log(data);    
}


// Formatacao do Texto de warning - Campo obrigatorio
function warningFormat(element){
    element.className = "text-danger text-left";
    return element;
}

// Mensagem de Campo obrigatorio
function warningMessageRequiredField(element){
  element.innerHTML = "(campo obrigatório)";
  return element;
}


function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    

}

function clearStorageOnBrowser(){
    localStorage.clear();
}
