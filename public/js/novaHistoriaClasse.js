//Variaveis Globais
var idClasse = 0;
var data;
let userId;

//autentificacao do usuario
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    userId =  user.uid;
    loadPage();    
  } else {
    window.location.href = "./Login.html";
  }
});


function loadPage(){
    var div = document.getElementById("data");
    data = localStorage.getItem("novaHistoria");  
}
  
function addClasse(){
    //caso esteja setado com a mensagem de alerta a div id=classeMessage volta a ser vazio clicarem no botão add
    document.getElementById('classeMessage').innerHTML = "";
    idClasse++;
    var itemClasse = `
    <div class="card-header" id="classe`+idClasse+`">                            
    <button class="btn btn-sm col-xl-11" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <input id="`+idClasse+`" class="form-control form-control-sm shadow" type="text" placeholder="Nome da Classe" >
        <div id="classeMessage`+idClasse+`" class="text-danger text-left"></div>
    </button>
    <i onclick="deleteClasse(classe`+idClasse+`)" class="far fa-trash-alt ml-3 fa-lg" ></i>
    </div>                                
    `
  
    document.getElementById("classes").insertAdjacentHTML('beforeend', itemClasse);
  
    //console.log(itemClasse);
    
}
  
function deleteClasse(id){
    id.remove();
    //classes.remove(novaClasse);
}

function next(){
    var validador = 0;
    if(document.getElementsByTagName('input').length == 0){
        document.getElementById('classeMessage').innerHTML = "Adicione pelo menos 1 classe!";
        validador++;
    }else{
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            if(document.getElementsByTagName('input')[x].value == ""){
                var id = document.getElementsByTagName('input')[x].id;
                document.getElementById('classeMessage'+id).innerHTML = "Campo Obrigatório!";
                validador++;
            }else{
                var id = document.getElementsByTagName('input')[x].id;
                document.getElementById('classeMessage'+id).innerHTML = "";
            }
        }
    }

    if(validador == 0){
        var classes = [];
        for(x=0; x < document.getElementsByTagName('input').length; x++){
            classes.push(document.getElementsByTagName('input')[x].value);
        }

        //tranformo os dados em objeto e adiciono o vetor ao objeto
        data = JSON.parse(data);
        data["classes"] = classes;
        
        //Despois de criar o json, salvo os dados no browser e mudo para a proxima página.
        localSaveStorage(data);
        window.location.href = 'novaHistoriaAtributoFixo.html';

        //console.log(data);
        //console.log(JSON.stringify(data));

    }
    
}

function clearStorageOnBrowser(){
    localStorage.clear();
}

function localSaveStorage(obj){        
    window.localStorage.setItem('novaHistoria', JSON.stringify(obj));    
}