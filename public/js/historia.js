async function loadPage(){
    var res = window.location.href.split('?'); 
    if (res[1] != undefined) {
        showRPG(await HistoryDAO.getById(res[1]));
    } else {
        window.location.href = "./biblioteca.html";
    }
}

async function showRPG(data) {
    var title = document.getElementById("title");
    title.textContent = data.nome;

    var description = document.getElementById("description");
    description.textContent = data.descricao;

    var author = document.getElementById("author");
    author.textContent = "Autor: " + await UserDAO.getName(data.autor);

    if (data.filtro != undefined) {
        var rating = document.getElementById("rating");
        if (data.filtro.avaliacao != undefined) {
            rating.textContent = "Nota: " + data.filtro.avaliacao;
        } else {
            rating.textContent = "Sem Avaliação";
        }

        var level = document.getElementById("level");
        if (data.filtro.dificuldade == "1") {
            level.textContent = "Fácil";
        } else if (data.filtro.dificuldade == "2") {
            level.textContent = "Intermediário";
        } else if (data.filtro.dificuldade == "3") {
            level.textContent = "Dificil";
        } 
        
        var time = document.getElementById("time");
        if (data.filtro.duracao == "1") {
            time.textContent = "+/- 30min";
        } else {
            time.textContent = "+/- " + (parseInt(data.filtro.duracao)-1).toString() + "h";
        }
        
        var player = document.getElementById("player");
        player.textContent = (parseInt(data.filtro.qtdjogador)+1).toString() + " ou + jogadores";
        
        var universe = document.getElementById("universe");
        universe.textContent = await HistoryDAO.getNameUniverse(data.filtro.universo);
    } else {
        var filters = document.getElementById("filters");
        filters.className = "d-none";
    }

    var atributos = document.getElementById("atributos");
    if (data.atributo_fixo != undefined) {
        loadAtributosFixos(atributos, data.atributo_fixo);
    } else {
        atributos.textContent = "Esta história não contém atributos fixos.";
    }

    var classes = document.getElementById("classes");
    if (data.classes != undefined) {
        loadClasseRaca(classes, data.classes);
    } else {
        classes.textContent = "Esta história não contém classes.";
    }

    var racas = document.getElementById("racas");
    if (data.racas != undefined) {
        loadClasseRaca(racas, data.racas);
    } else {
        racas.textContent = "Esta história não contém raças.";
    }

    if (data.npc != undefined) {
        loadNPCs(data.npc, data.atributo_fixo);
    }  else {
        var div = document.getElementById("accordion");
        div.className = "card p-3";
        div.textContent = "Esta história não contém nenhum NPC.";
    }
}

function loadClasseRaca(div, data) {
    div.className = "d-flex ml-5 mt-2";
    var titulos = document.createElement('div');
    for (var a in data) {
        var p = document.createElement('p');
        p.textContent = data[a];
        titulos.appendChild(p);
    }
    div.appendChild(titulos);
}

function loadAtributosFixos(div, atributos) {
    div.className = "d-flex ml-5 mt-2";
    var titulos = document.createElement('div');
    var limites =document.createElement('div');
    limites.className = "ml-3";
    for (var a in atributos) {
        var p = document.createElement('p');
        p.textContent = atributos[a].nome ;
        titulos.appendChild(p);
        var d = document.createElement('p');
        d.textContent =  atributos[a].max;
        limites.appendChild(d);
    }
    div.appendChild(titulos);
    div.appendChild(limites);
}

function loadNPCs(npcs, fixos) {
    var i=4;
    var div = document.getElementById("accordion");

    for (var n in npcs) {
        i++;
        var card = document.createElement('div');
        card.className = "card historia";

        var cardHeader = document.createElement('div');
        cardHeader.className = "card-header";
        cardHeader.id = "heading"+i;

        var h4 = document.createElement('h2');
        h4.className = "mb-0";

        var btnCollapse = document.createElement('button');
        btnCollapse.className = "btn btn-link collapsed text-dark text-left w-100 text-decoration-none";
        btnCollapse.setAttribute("data-toggle","collapse");
        btnCollapse.setAttribute("data-target","#collapse"+i);
        btnCollapse.setAttribute("aria-expanded","false");
        btnCollapse.setAttribute("aria-controls","collapse"+i);
        btnCollapse.textContent = npcs[n].nome;

        var descr = document.createElement('div');
        descr.id = "collapse"+i;
        descr.className = "collapse";
        descr.setAttribute("aria-labelledby","heading"+i);
        descr.setAttribute("data-parent","#accordion");

        var descrBody = document.createElement('div');
        descrBody.className = "card-body";
        var detalhes = document.createElement('div');
        detalhes.className = "font-italic";
        detalhes.textContent = npcs[n].descricao;
        var atributos = document.createElement('div');
        loadAtributos(atributos, npcs[n].atributos, fixos);
        descrBody.appendChild(detalhes);
        descrBody.appendChild(atributos);

        // Add card to de page
        h4.appendChild(btnCollapse);
        cardHeader.appendChild(h4);
        descr.appendChild(descrBody);
        card.appendChild(cardHeader);
        card.appendChild(descr);

        // Add card to de page
        div.appendChild(card);
    }
}

function loadAtributos(div, atributos, fixos) {
    div.className = "d-flex ml-4 mt-2";
    var titulos = document.createElement('div');
    var limites = document.createElement('div');
    limites.className = "ml-3";
    for (var a in atributos) {
        var p = document.createElement('p');
        p.textContent = fixos[a].nome ;
        p.className = "p-2";
        titulos.appendChild(p);
        var d = document.createElement('p');
        d.textContent =  atributos[a];
        d.className = "border p-2";
        limites.appendChild(d);
    }
    div.appendChild(titulos);
    div.appendChild(limites);
}

async function newSave() {
    let idHistoria = window.location.href.split('?')[1];
    window.location.href = './inicioPartida.html?'+idHistoria;
}

function navegar(){
    window.location.href = "./navegar.html?" + window.location.href.split('?')[1];
}