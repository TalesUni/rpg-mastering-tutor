function controlForm(){
    const emailElem = document.getElementById("input_email")
    const senhaElem = document.getElementById("input_senha")

    document.getElementById("input_senha")
        .addEventListener("keyup", ev => {
            // Número 13 corresponde ao "Enter"
            if (ev.keyCode === 13) {
                ev.preventDefault();
                entrar(emailElem.value, senhaElem.value);
            }
        });

    document.getElementById("btn_entrar")
        .addEventListener("click", ev => {
            ev.preventDefault();
            entrar(emailElem.value, senhaElem.value);
        });
}

function entrar (email, senha){
    window.UserDAO.login.call(this, email, senha).catch( err => {
        console.error(err);
        alert("Houve um erro: " + err.message);
    });
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        window.location.href = "./index.html";
    }
});
