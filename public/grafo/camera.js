if (window.Global === undefined) window.Global = {};

// Classe Camera
(function() {
    function Camera(initX, initY, world) {
        this.x = initX;
        this.y = initY;
        this.world = world;

        this.maxZoom = 2;
        this.minZoom = 0.5;
        this.zoom = 1;

        //Configurando movimentação
        this.gatilho = false;
        this.objStart = { x: null, y: null};
        this.mouseStart = { x: null, y: null};
        this.target = { x: null, y: null};

        window.addEventListener("canvas-mousedown",ev => {
            this.gatilho = true;
            this.objStart.x = this.x;
            this.objStart.y = this.y;
            this.target.x = this.x;
            this.target.y = this.y;
            this.mouseStart.x = ev.detail.x;
            this.mouseStart.y = ev.detail.y;
        }, false);

        window.addEventListener("canvas-mousemove", ev => {
            if (! this.gatilho) return;
            let delta = { x: null, y: null};
            delta.x = ev.detail.x - this.mouseStart.x;
            delta.y = ev.detail.y - this.mouseStart.y;

            //Isso é necessário para dar
            //a sensação de arrastar
            delta.x *= -1 / this.zoom;
            delta.y *= -1 / this.zoom;

            this.target.x = this.objStart.x + delta.x;
            this.target.y = this.objStart.y + delta.y;
        }, false);

        window.addEventListener("mouseup", _ => this.gatilho = false, false);

        window.addEventListener("canvas-wheel", ev => {
            this.zoom +=  -0.05 * Math.sign(ev.detail.deltaY);
            this.zoom = this.zoom < this.minZoom ? this.minZoom : this.zoom;
            this.zoom = this.zoom > this.maxZoom ? this.maxZoom : this.zoom;

        }, false);
    }

    Camera.prototype.update = function(world) {
        if(!this.gatilho) return;
        if (world !== undefined) this.world = world;

        let delta = {x: null, y: null};
        delta.x = this.target.x - this.x;
        delta.y = this.target.y - this.y;

        this.x += delta.x;
        this.y += delta.y;

        // mantendo a camera nos limites do mundo
        if (this.x < this.world.x.start) this.x = this.world.x.start;
        if (this.x > this.world.x.end/this.zoom) this.x = this.world.x.end/this.zoom;

        if (this.y < this.world.y.start) this.y = this.world.y.start;
        if (this.y > this.world.y.end) this.y = this.world.y.end;
    }

    // Desenha na perspectiva da camera
    Camera.prototype.draw = function(ctx, draw) {
        ctx.save();
        ctx.scale(this.zoom, this.zoom);
        ctx.translate(-this.x, -this.y);
        draw(ctx);
        ctx.restore();
    }

    Global.Camera = Camera;
})();
