if (window.Global === undefined) window.Global = {};

// Cria Utils
(function () {
    Global.Utils = {};

    Global.Utils.cenasToGrafo = function (cenas, idSelecionado = 0){
        const root = cenas[0];

        cenas = Object.values(cenas);
        for (let cena of cenas) {
            if (cena.proximo === undefined) continue;
            cena.proximo = Object.values(cena.proximo);
        }

        (function defineIds(cenas) {
            let count = 0;
            for (const cena of cenas) {
                cena.id = count;
                count ++;
            }
        })(cenas);
        (function indexaFilhos(cena) {
            if(cena.proximo === undefined) {
                cena.filhos = [];
                return;
            }
            if(cena.filhos !== undefined) return;

            cena.filhos = cena.proximo.map(index => cenas[index]);
            for (const proximo of cena.proximo) {
                indexaFilhos(cenas[proximo], cena);
            }
        })(root);
        (function indexaPais(cena) {
            for (const filho of cena.filhos) {
                if(filho.pais === undefined) {
                    filho.pais = [cena];
                    indexaPais(filho, cena);
                }
                else filho.pais.push(cena);

            }
        })(root);
        (function defineAltura(cena, alturaAtual){
            if (cena.altura !== undefined){
                if (cena.altura < alturaAtual){
                    cena.altura = alturaAtual
                }
            }
            else {
                cena.altura = alturaAtual;
            }
            for (const filho of cena.filhos) {
                defineAltura(filho, alturaAtual + 1);
            }
        })(root, 0);

        const distPorLinha = 100;
        const dispersor = 5;
        const espacador = 200;

        let nLinha = 0;
        let tabela = [];

        tabela[nLinha] = [root];
        root.x = 0;
        root.y = 0;

        while(tabela[nLinha].length !== 0){
            let linhaAtual = tabela[nLinha]
            nLinha += 1

            tabela[nLinha] = [];
            let proximaLinha = tabela[nLinha];

            for (const cena of linhaAtual) {
                let tam = cena.filhos.length;
                let ang = tam/2 - tam + 0.5;

                for (const filho of cena.filhos) {
                    if(cena.altura !== Math.max(...filho.pais.map(pai => pai.altura))){
                        continue;
                    }

                    if (proximaLinha.includes(filho)) continue;
                    else proximaLinha.push(filho);

                    filho.x = filho.pais.map(pai => pai.x).reduce((acc, val) => acc+val)/filho.pais.length;
                    filho.x += dispersor * Math.sign(filho.x);
                    filho.x += espacador * ang;
                    filho.y = nLinha * distPorLinha;

                    ang += 1;
                }
            }
        }

        let vertices = [], arestas = [];
        (function constroiGrafo(cena, pai) {
            let ver = new Global.Vertice(cena.x, cena.y, cena.titulo, cena.id)

            const tempVer = vertices.find(elem => elem.id === ver.id)
            if(tempVer !== undefined){
                ver = tempVer;
            }
            else{
                vertices.push(ver);
                for (const filho of cena.filhos) {
                    constroiGrafo(filho, ver);
                }
            }
            if(pai !== null) arestas.push(new Global.Aresta(pai, ver));
        })(root, null);

        for (const vertice of vertices) {
            if(vertice.id === idSelecionado){
                vertice.selecionado = true;
                break;
            }
        }

        return new Global.Grafo(vertices, arestas);
    }

    Global.Utils.registrarEventos = function (canvas, getMargem) {

        canvas.addEventListener('mousemove', ev => {
            window.dispatchEvent(new CustomEvent('canvas-mousemove', {
                detail : {
                    x: ev.x - getMargem().x,
                    y: ev.y - getMargem().y,
                }
            }));
        });

        canvas.addEventListener('mousedown', ev => {
            window.dispatchEvent(new CustomEvent('canvas-mousedown', {
                detail : {
                    x: ev.x - getMargem().x,
                    y: ev.y - getMargem().y,
                }
            }));
        });

        canvas.addEventListener('mouseup', ev => {
            window.dispatchEvent(new CustomEvent('canvas-mouseup', {
                detail : {
                    x: ev.x - getMargem().x,
                    y: ev.y - getMargem().y,
                }
            }));
        });

        canvas.addEventListener('wheel', ev => {
            ev.preventDefault();
            window.dispatchEvent(new CustomEvent('canvas-wheel', {
                detail : {
                    deltaY: ev.deltaY,
                }
            }));
        });
    }
})();

Global.main = (async function main(idHistoria, idElemento, tamanho, cenas = null, estado = null) {
    // setup
    const canvas = document.getElementById(idElemento);
    const ctx = canvas.getContext("2d");
    canvas.width = tamanho;
    canvas.height = tamanho;

    let getMargem = () => {
        return {
            x: canvas.getBoundingClientRect().left - canvas.clientLeft,
            y: canvas.getBoundingClientRect().top - canvas.clientTop,
        }
    }

    Global.Utils.registrarEventos(canvas, getMargem)

    // configurações
    const FPS = 90;
    const INTERVAL = 1000 / FPS; // milisegundos

    if (cenas === null) {
        await window.HistoryDAO.getById(idHistoria)
            .then((historia) => {
                cenas = historia.cenas
            })
            .catch((err) => {
                alert("Erro ao carregar a História");
                console.error(err);
            });
    }

    let grafo;
    let inicio = {
        x: -canvas.width/2,
        y: -canvas.height/2,
    };

    if (estado === null ){
        grafo = Global.Utils.cenasToGrafo(cenas);
    }
    else{
        grafo = Global.Utils.cenasToGrafo(cenas, estado.idSelecionado);
        inicio = estado.posicao;
    }

    let world = {
        x: {
            start: grafo.width.min - canvas.width,
            end: grafo.width.max,
        },
        y: {
            start: grafo.height.min - canvas.height,
            end: grafo.height.max,
        }
    }
    const camera = new Global.Camera(inicio.x, inicio.y, world);

    const update = function() {
        world.y.start = grafo.height.min - canvas.height/camera.zoom
        world.x.start = grafo.width.min - canvas.width/camera.zoom
        world.x.end = grafo.width.max * camera.zoom
        camera.update(world);

        grafo.update(camera.x, camera.y, camera.zoom);
    }

    const draw = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Deixa a camera decidir como desenhar
        camera.draw(ctx , () => {
            grafo.draw(ctx);
        });
    }

    // Loop Principal
    const gameLoop = function() {
        update();
        draw();
    }

    // Executa tudo
    setInterval(function() {
        gameLoop();
    }, INTERVAL);

    return {
        getEstado: () => {
            let idSelecionado;
            for (const vertice of grafo.vertices) {
                if(vertice.selecionado){
                    idSelecionado = vertice.id;
                    break;
                }
            }

            let posicao;
            posicao = {
                x: camera.x,
                y: camera.y
            }

            return {
                idSelecionado,
                posicao
            }
        }
    }
});

/*
window.onload = (() => {
    const id = window.location.href.split('?')[1];
    if (id !== undefined){
        Global.main(id, "canvasGrafo", 500);
    } else {
        window.location.href = "./biblioteca.html";
    }
});
 */
