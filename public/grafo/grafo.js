if (window.Global === undefined) window.Global = {};

// Classe Vertice
(function (){
    function Vertice(x, y, titulo, id) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.raio = 20;
        this.titulo = titulo;
        this.mostrarTitulo = false;
        this.selecionado = false;

        this.offsetX = 0;
        this.offsetY = 0;
        this.zoom    = 1;

        this.click = {}


        window.addEventListener('canvas-mousemove' ,ev => {
            this.mostrarTitulo = !!(this.proximo(ev.detail.x, ev.detail.y));
        }, false);

        window.addEventListener('canvas-mousedown', ev => {
            if(!!(this.proximo(ev.detail.x, ev.detail.y))) {
                this.click = {
                    gatilho: true,
                    x: ev.detail.x,
                    y: ev.detail.y,
                }
            }
        }, true);

        window.addEventListener('canvas-mouseup', ev => {
            if(this.click.gatilho && !!this.proximo(ev.detail.x, ev.detail.y)){
                let dist = 5;
                const delta = {
                    x : Math.abs(this.click.x - ev.detail.x),
                    y : Math.abs(this.click.y - ev.detail.y),
                };
                if (delta.y < dist && delta.x < dist){
                    window.dispatchEvent(new CustomEvent('verticeClicado', {
                        detail : {
                            id: this.id
                        }
                    }));
                }
            }
            this.click.gatilho = false;
        }, true);

        window.addEventListener('verticeClicado',ev => this.selecionado = ev.detail.id === this.id, false);
    }
    Vertice.prototype.draw = function (ctx) {

        if (this.selecionado){
            ctx.save();
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.raio + 5, 0, Math.PI * 2, false);
            ctx.fillStyle = '#2C378A';
            ctx.fill();
            ctx.restore();
        }

        ctx.save();
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.raio, 0, Math.PI * 2, false);
        ctx.fillStyle = '#495BE3';
        ctx.fill();
        ctx.restore();

        if(this.mostrarTitulo){
            ctx.save();
            ctx.beginPath();
            ctx.rect(this.x + this.raio, this.y - this.raio - 17.5, ctx.measureText(this.titulo).width * 1.75, 15);
            ctx.fillStyle = '#FFF';
            ctx.fill();
            ctx.fillStyle = '#003';
            ctx.font = "bold 15px Sans";
            ctx.fillText(this.titulo, this.x + this.raio, this.y - this.raio - 5);
            ctx.restore();
        }

    }

    Vertice.prototype.update = function (offsetX, offsetY, zoom){
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.zoom    = zoom;
    }

    Vertice.prototype.proximo = function (x, y){
        const real = {
            x : x/this.zoom + this.offsetX,
            y : y/this.zoom + this.offsetY
        }

        let dist = this.raio + 1;
        let delta = {
            x : Math.abs(this.x - real.x),
            y : Math.abs(this.y - real.y),
        };
        return (delta.y < dist && delta.x < dist);
    }

    Vertice.prototype.equals = function (obj){
        return this.id === obj.id;
    }

    Global.Vertice = Vertice;
})();

// Classe Aresta
(function() {
    function Aresta(origem, destino) {
        this.origem = origem;
        this.destino = destino;
    }

    Aresta.prototype.draw = function(ctx) {
        ctx.beginPath();
        ctx.moveTo(this.origem.x, this.origem.y);
        ctx.lineTo(this.destino.x, this.destino.y);
        ctx.strokeStyle = 'grey';
        ctx.lineWidth = 5;
        ctx.stroke();
    }

    Aresta.prototype.equals = function (obj){
        return (this.origem.equals(obj.origem) && this.destino.equals(obj.destino));
    }

    Global.Aresta = Aresta;
})();

// Classe Grafo
(function() {
    function Grafo(vertices, arestas){
        this.vertices = vertices;
        this.arestas  = arestas;

        let maxW = 0, minW = 0, maxH = 0;
        for (const vertice of vertices){
            if (vertice.y > maxH) maxH = vertice.y;

            if (vertice.x > maxW) maxW = vertice.x;
            if (vertice.x < minW) minW = vertice.x;
        }
        this.width = {
            min: minW,
            max: maxW,
        }
        this.height = {
            min: 0,
            max: maxH,
        }
    }

    Grafo.prototype.draw = function(ctx){
        for (const aresta of this.arestas) {
            aresta.draw(ctx);
        }

        for (const vertice of this.vertices) {
            vertice.draw(ctx);
        }
    }

    Grafo.prototype.update = function(offsetX, offsetY, zoom){
        for (const vertice of this.vertices) {
            vertice.update(offsetX, offsetY, zoom);
        }
    }

    Global.Grafo = Grafo;
})();
