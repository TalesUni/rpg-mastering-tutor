this.UserDAO = {};

////////////////// GET GERAL ////////////////////
/*      getUserById(id)
*  Entrada: id do usuario logado
*  Saida: Todos os dados (nao secretos) do usuario logado
*/
(function (that) {
    async function getUserById(id){
        let user = firebase.database().ref('usuarios/' + id);
        return await user.once('value').then( (snapshot) => {
            return snapshot.val();
        }).catch( (err) => {
            alert("Erro ao carregar usuario "+id);
            console.error(err);
        })
    }
    that.UserDAO.getUserById = getUserById;
})(this);

/*      getName(id)
*  Entrada: id do usuario logado
*  Saida: Nome do usuario logado
*/
(function (that) {
    async function getName(id){
        let data = await UserDAO.getUserById(id);
        return data.nome;
    }
    that.UserDAO.getName = getName;
})(this);

/*      getEmail(id)
*  Entrada: id do usuario logado
*  Saida: E-mail do usuario logado
*/
(function (that) {
    async function getEmail(id){
        let data = await UserDAO.getUserById(id);
        return data.email;
    }
    that.UserDAO.getEmail = getEmail;
})(this);

/*      getAbout(id)
*  Entrada: id do usuario logado
*  Saida: Sobre do usuario logado
*/
(function (that) {
    async function getAbout(id){
        let data = await UserDAO.getUserById(id);
        return data.sobre;
    }
    that.UserDAO.getAbout = getAbout;
})(this);

/*      getAllSaves(id)
*  Entrada: id do usuario logado
*  Saida: Lista dos jogos salvos do usuario logado
*/
(function (that) {
    async function getAllSaves(id){
        let data = await UserDAO.getUserById(id);
        let saves = [];
        for (let a in data.saves) {
            let b = Object.assign({},{id: a},data.saves[a]);
            saves.push(b);
        }
        return saves;
    }
    that.UserDAO.getAllSaves = getAllSaves;
})(this);


/*      getMyRPGs(id)
*  Entrada: id do usuario logado
*  Saida: Lista das historias criadas pelo usuario logado
*/
(function (that) {
    async function getMyRPGs(id){
        let data = await UserDAO.getUserById(id);
        let saves = [];
        for (let a in data.minhas) {
            saves.push(data.minhas[a]);
        }
        return saves;
    }
    that.UserDAO.getMyRPGs = getMyRPGs;
})(this);


////////////// UPDATES ////////////////////

/*      updateName(id, name)
*  Entrada: id do usuario logado e novo 'nome'
*  Saida: Mensagem de sucesso ou erro
*/
(function (that) {
    async function updateName(id, name){
        firebase.database().ref('usuarios/' + id + '/nome').set(name, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel atualizar");
            } else {
                alert("'Nome' atualizado com sucesso!");
            }
        });
    }
    that.UserDAO.updateName = updateName;
})(this);

/*      updateAbout(id, newAbout)
*  Entrada: id do usuario logado e novo 'sobre mim'
*  Saida: Mensagem de sucesso ou erro
*/
(function (that) {
    async function updateAbout(id, newAbout){
        firebase.database().ref('usuarios/' + id + '/sobre').set(newAbout, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel atualizar");
            } else {
                alert("'Sobre mim' atualizado com sucesso!");
            }
        });
    }
    that.UserDAO.updateAbout = updateAbout;
})(this);


////////////// Login ////////////////////

/*      login(email, senha)
*  Entrada: email e a senha do usuário
*  Saida: Promise de autorização do usuário
*/

(function (that){
    async function login(email, senha){
        return await this.firebase.auth().signInWithEmailAndPassword(email, senha);
    }
    that.UserDAO.login = login
})(this);


////////////// Logout ////////////////////

/*      logout(email, senha)
*  Entrada: email do usuário
*  Saida: Promise de saida do usuário
*/

(function (that){
    async function logout(email){
        const usuario =  await this.firebase.auth().currentUser;

        if (!usuario) return Promise.reject("Usuário já deslogado");
        if (usuario.email !== email) return Promise.reject("Email incorreto");

        return this.firebase.auth().signOut();
    }
    that.UserDAO.logout = logout;
})(this);


////////////// SignIn ////////////////////

/*      signIn(nome, email, senha)
*  Entrada: nome,email e senha do novo usuário
*  Saida: Promise de cadastro do usuário
*/

(function (that){
    async function signUp(nome, email, senha, senhaConf){
        if(nome === "" || nome === undefined) return Promise.reject("Nome inválido");
        if(senha !== senhaConf) return Promise.reject("Senhas não conferem");
        if(! email.match(/([\w\d]+@[\w\d]+(\.[\w\d]+)+)/g)) return Promise.reject("Email em formato inválido");

        await this.firebase.auth().createUserWithEmailAndPassword(email, senha);
        let bd_ref = await this.firebase.database().ref();
        let updates = {};
        updates['/usuarios/' + await this.firebase.auth().currentUser.uid] = {email: email, nome: nome};

        return await bd_ref.update(updates);
    }
    that.UserDAO.signUp = signUp;
})(this);
