window.HistoryDAO = {};

/////////////////// GETs  ///////////////////
/*      getAll(id)
*  Entrada: nenhuma
*  Saida: Lista de todas as historias disponiveis no sistema
*/
(function () {
    async function getAll(){
        let rpgs = firebase.database().ref('historias');
        return await rpgs.once('value').then( (snapshot) => {
            let historias = [];
            snapshot.forEach( (childSnapshot) => {
                let data = Object.assign({},{id: childSnapshot.ref.path.pieces_[1]}, childSnapshot.val());
                historias.push(data);
            })
            return historias;
        }).catch( (err) => {
            alert("Erro ao carregar histórias");
            console.error(err);
        })
    }
    window.HistoryDAO.getAll = getAll;
})();

/*      getByName(id)
*  Entrada: Nome da historia procurada
*  Saida: Dados da historia em questao
*/
(function () {
    async function getByName(nome){
        let rpgs = firebase.database().ref('historias');
        return await rpgs.once('value').then( (snapshot) => {
            let historia = null;
            snapshot.forEach( (childSnapshot) => {
                let data = childSnapshot.val();
                if(nome === data.nome){
                    historia = data;
                }
            })
            return historia;
        }).catch( (err) => {
            alert("Erro ao carregar a história: "+nome);
            console.error(err);
        })
    }
    window.HistoryDAO.getByName = getByName;
})();

/*      getById(id)
*  Entrada: Id da historia procurada
*  Saida: Dados da historia em questao
*/
(function () {
    async function getById(id){
        let bd = firebase.database().ref("historias/"+id);
        let ret = await bd.once('value');
        return JSON.parse(JSON.stringify(ret));
    }
    window.HistoryDAO.getById = getById;
})();

/*      getJsonById(id)
*  Entrada: Id da historia procurada
*  Saida: Dados da historia em questao
*/
(function () {
    async function getJsonById(id){
        let historia = firebase.database().ref('historias/' + id);
        return await historia.once('value').then( (snapshot) => {
            return snapshot.val();
        }).catch( (err) => {
            alert("Erro ao carregar historia "+id);
            console.error(err);
        })
    }
    window.HistoryDAO.getJsonById = getJsonById;
})();

/*      getName(id)
*  Entrada: Id da historia procurada
*  Saida: Nome da historia em questao
*/
(function () {
    async function getName(id) {
        let data = await HistoryDAO.getById(id);
        return data.nome;
    }
    window.HistoryDAO.getName = getName;
})();

/*      getDescription(id)
*  Entrada: Id da historia procurada
*  Saida: Descricao da historia em questao
*/
(function () {
    async function getDescription(id) {
        let data = await HistoryDAO.getById(id);
        return data.descricao;
    }
    window.HistoryDAO.getDescription = getDescription;
})();

/*      getAuthor(id)
*  Entrada: Id da historia procurada
*  Saida: Descricao da historia em questao
*/
(function () {
    async function getAuthor(id) {
        let data = await HistoryDAO.getById(id);
        return data.autor;
    }
    window.HistoryDAO.getAuthor = getAuthor;
})();

/*      getRules(id)
*  Entrada: Id da historia procurada
*  Saida: Regra na hora da criacao de um jogo da historia em questao
*/
(function () {
    async function getRules(id) {
        let data = await HistoryDAO.getById(id);
        return data.regras;
    }
    window.HistoryDAO.getRules = getRules;
})();

/*      getFixedAtributes(id)
*  Entrada: Id da historia procurada
*  Saida: Lista dos atributos fixos da historia em questao
*/
(function () {
    async function getFixedAtributes(id) {
        let data = await HistoryDAO.getById(id);
        let atributos = [];
        for (let a in data.atributo_fixo) {
            atributos.push(data.atributo_fixo[a]);
        }
        return atributos;
    }
    window.HistoryDAO.getFixedAtributes = getFixedAtributes;
})();

/*      getCenas(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das cenas da historia em questao
*/
(function () {
    async function getCenas(id) {
        let data = await HistoryDAO.getById(id);
        let cenas = [];
        for (let a in data.cenas) {
            cenas.push(data.cenas[a]);
        }
        return cenas;
    }
    window.HistoryDAO.getCenas = getCenas;
})();

/*      getCena(idHistoria, idCena)
*  Entrada: Id da historia procurada
*  Saida: Lista das cenas da historia em questao
*/
(function () {
    async function getCena(idHistoria, idCena) {
        let cenas = await HistoryDAO.getCenas(idHistoria);

        if (idCena >= cenas.length)
            return null;
        else
            return cenas[idCena];
    }
    window.HistoryDAO.getCena = getCena;
})();

/*      getClasses(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das classes da historia em questao
*/
(function () {
    async function getClasses(id) {
        let data = await HistoryDAO.getById(id);
        let classes = [];
        for (let a in data.classes) {
            classes.push(data.classes[a]);
        }
        return classes;
    }
    window.HistoryDAO.getClasses = getClasses;
})();

/*      getRacas(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das racas da historia em questao
*/
(function () {
    async function getRacas(id) {
        let data = await HistoryDAO.getById(id);
        let racas = [];
        for (let a in data.racas) {
            racas.push(data.racas[a]);
        }
        return racas;
    }
    window.HistoryDAO.getRacas = getRacas;
})();

/*      getNPCs(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das racas da historia em questao
*/
(function () {
    async function getNPCs(id) {
        let data = await HistoryDAO.getById(id);
        let npcs = [];
        for (let a in data.npc) {
            npcs.push(data.npc[a]);
        }
        return npcs;
    }
    window.HistoryDAO.getNPCs = getNPCs;
})();

/*      getNPC(idHistoria, idNPC)
*  Entrada: Id da historia e id do NPC procurado
*  Saida: Informacoes do NPC procurado
*/
(function () {
    async function getNPC(idHistoria, idNPC) {
        let npcs = await HistoryDAO.getNPCs(idHistoria);

        if (idNPC > npcs.length)
            return null;
        else
            return npcs[idNPC];
    }
    window.HistoryDAO.getNPC = getNPC;
})();

/*      getFilters(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das racas da historia em questao
*/
(function () {
    async function getFilters(id) {
        let data = await HistoryDAO.getById(id);
        let filters = [];
        for (let a in data.filtro) {
            let b = {tipo : a, valor : data.filtro[a]};
            filters.push(b);
        }
        return filters;
    }
    window.HistoryDAO.getFilters = getFilters;
})();

/////////////////// UPDATE //////////////////////

/*      updateRate(id)
*  Entrada: Id da historia procurada
*  Saida: Lista das racas da historia em questao
*/
(function () {
    async function updateRate(id, nota) {
        let data = await HistoryDAO.getById(id);

        let avaliacao;
        if (data.filtro.avaliacao != undefined) {
            avaliacao = (data.filtro.avaliacao + nota)/2;
        } else {
            avaliacao = nota;
        } 

        let postsRef = firebase.database().ref('historias/'+id+'/filtro/avaliacao');

        await postsRef.set(avaliacao, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar a nota!");
            }
        });

        let reviews;
        if (data.reviews != undefined) {
            reviews = data.reviews +1;
        } else {
            reviews = 1;
        }        

        postsRef = firebase.database().ref('historias/'+id+'/reviews');

        await postsRef.set(reviews, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar o review!");
            }
        });
    }
    window.HistoryDAO.updateRate = updateRate;
})();

/////////////////// CREATE //////////////////////

/*      newMinha(idUser, idHistoria)
*  Entrada: id do usuario logado e id da historia que o usuario quer jogar
*  Saida: Id do save desta historia criado para o usuario logado
*/
(function () {
    async function newMinha(idUser, data){
        // Add the new History in the database
        let dataComplete = Object.assign({},{autor: idUser},data);
        let postsRef = firebase.database().ref('historias');

        let newPostRef = postsRef.push();
        await newPostRef.set(dataComplete, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel criar uma nova historia!");
            }
        });
        let idPost = newPostRef.getKey();

        // Add the new RPG in the author's list
        let ref = firebase.database().ref('usuarios/'+idUser+'/minhas');

        let newMine = ref.push();
        await newMine.set({idHistoria: idPost}, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel adicionar a nova historia em sua lista!");
            }
        });

        return idPost;
    }
    window.HistoryDAO.newMinha = newMinha;
})();


/////////////////// FILTERS ///////////////////

/*      getAllFilters()
*  Entrada: nenhuma
*  Saida: Lista de todos os filtros do sistema
*/
(function () {
    async function getAllFilters(){
        let unis = firebase.database().ref('filtros');
        return await unis.once('value').then( (snapshot) => {
            let filtros = [];
            snapshot.forEach( (childSnapshot) => {
                filtros.push(childSnapshot.val());
            })
            return filtros;
        }).catch( (err) => {
            alert("Erro ao carregar filtros");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllFilters = getAllFilters;
})();

/*      getAllDuration()
*  Entrada: nenhuma
*  Saida: Lista de todas as duracoes possiveis para a historia
*/
(function () {
    async function getAllDuration(){
        let unis = firebase.database().ref('filtros/duracao');
        return await unis.once('value').then( (snapshot) => {
            let duracao = [];
            snapshot.forEach( (childSnapshot) => {
                duracao.push(childSnapshot.val());
            })
            return duracao;
        }).catch( (err) => {
            alert("Erro ao carregar duracao");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllDuration = getAllDuration;
})();

/*      getAllDifficulty()
*  Entrada: nenhuma
*  Saida: Lista de todas as dificuldades possiveis para a historia
*/
(function () {
    async function getAllDifficulty(){
        let unis = firebase.database().ref('filtros/dificuldade');
        return await unis.once('value').then( (snapshot) => {
            let dificuldade = [];
            snapshot.forEach( (childSnapshot) => {
                dificuldade.push(childSnapshot.val());
            })
            return dificuldade;
        }).catch( (err) => {
            alert("Erro ao carregar dificuldade");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllDifficulty = getAllDifficulty;
})();

/*      getAllRatings()
*  Entrada: nenhuma
*  Saida: Lista de filtros de avaliacoes possiveis das historia
*/
(function () {
    async function getAllRatings(){
        let unis = firebase.database().ref('filtros/avaliacao');
        return await unis.once('value').then( (snapshot) => {
            let avaliacao = [];
            snapshot.forEach( (childSnapshot) => {
                avaliacao.push(childSnapshot.val());
            })
            return avaliacao;
        }).catch( (err) => {
            alert("Erro ao carregar avaliacoes");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllRatings = getAllRatings;
})();

/*      getAllQttPlayer()
*  Entrada: nenhuma
*  Saida: Lista de todas as quantidades de jogadores possiveis para a historia
*/
(function () {
    async function getAllQttPlayer(){
        let unis = firebase.database().ref('filtros/qtdjogador');
        return await unis.once('value').then( (snapshot) => {
            let qtdjogador = [];
            snapshot.forEach( (childSnapshot) => {
                qtdjogador.push(childSnapshot.val());
            })
            return qtdjogador;
        }).catch( (err) => {
            alert("Erro ao carregar qtdjogador");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllQttPlayer = getAllQttPlayer;
})();

/*      getAllUniverses(id)
*  Entrada: nenhuma
*  Saida: Lista de todos os universos disponiveis no sistema
*/
(function () {
    async function getAllUniverses(){
        let unis = firebase.database().ref('filtros/universo');
        return await unis.once('value').then( (snapshot) => {
            let universos = [];
            snapshot.forEach( (childSnapshot) => {
                universos.push(childSnapshot.val());
            })
            return universos;
        }).catch( (err) => {
            alert("Erro ao carregar universos");
            console.error(err);
        })
    }
    window.HistoryDAO.getAllUniverses = getAllUniverses;
})();

/*      getUniverseById(id)
*  Entrada: Id do universo que esta sendo procurado
*  Saida: Dados do universo em questao
*/
(function () {
    async function getUniverseById(id) {
        let uni = firebase.database().ref('filtros/universo/'+id);
        return await uni.once('value').then( (snapshot) => {
            return snapshot.val();
        }).catch( (err) => {
            alert("Erro ao carregar  universo");
            console.error(err);
        })
    }
    window.HistoryDAO.getUniverseById = getUniverseById;
})();

/*      getNameUniverse(id)
*  Entrada: Id do universo que esta sendo procurado
*  Saida: Nome do universo em questao
*/
(function () {
    async function getNameUniverse(id){
        let universo = await HistoryDAO.getUniverseById(id);
        return universo.nome;
    }
    window.HistoryDAO.getNameUniverse = getNameUniverse;
})();

