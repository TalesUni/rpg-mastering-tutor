window.MatchDAO = {};

//////////////// SAVES //////////////////
/*      getSaveById(idUser, idSave)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Jogo salvo do usuario logado
*/
(function () {
    async function getSaveById(idUser, idSave){
        let data = await UserDAO.getUserById(idUser);
        for (s in data.saves) {
            if (s == idSave) {
                return data.saves[s];
            }
        }
        return null;
    }
    window.MatchDAO.getSaveById = getSaveById;
})();

/*      delSave(idUser, idSave)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Jogo salvo do usuario logado
*/
(function () {
    async function delSave(idUser, idSave){
        let user = firebase.database().ref('usuarios/' + idUser + '/saves/' + idSave).set({
            idHistoria: null
        }, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel delete save!");
            }
        });
    }
    window.MatchDAO.delSave = delSave;
})();

/*      newSave(idUser, idHistoria)
*  Entrada: id do usuario logado e id da historia que o usuario quer jogar
*  Saida: Id do save desta historia criado para o usuario logado
*/
(function () {
    async function newSave(idUser, idH){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves');

        let newPostRef = postsRef.push();
        newPostRef.set({
            idHistoria: idH,
            noAtual: 0
        }, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel adicionar save!");
            }
        });

        // Cria uma copia dos npcs da historia no save
        let key = newPostRef.getKey();
        let npcs = await HistoryDAO.getNPCs(idH);
        let save = firebase.database().ref('usuarios/'+idUser+'/saves/'+key+'/npcs');
        save.set(npcs, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel adicionar NPCs no save!");
            }
        });

        return key;
    }
    window.MatchDAO.newSave = newSave;
})();

/*      getAllPlayers(idUser, idSave)
*  Entrada: id do usuario logado e id do jogo salvo
*  Saida: Lista dos players do jogo salvo em questao do usuario logado
*/
(function () {
    async function getAllPlayers(idUser, idSave){
        let save = await MatchDAO.getSaveById(idUser, idSave);
        let players = [];
        for (let a in save.players) {
            let b = {id: a};
            players.push(Object.assign({},b,save.players[a]));
        }
        return players;
    }
    window.MatchDAO.getAllPlayers = getAllPlayers;
})();

/*      addPlayerInSave(idUser, idSave, data)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Id do novo player criado
*/
(function () {
    async function addPlayerInSave(idUser, idSave, data){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/players");

        let newPostRef = postsRef.push();
        newPostRef.set(data, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel adicionar Player!");
            }
        });

        return newPostRef.getKey();
    }
    window.MatchDAO.addPlayerInSave = addPlayerInSave;
})();

/*      changeAtributePlayer(idUser, idSave, data)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Id do novo player criado
*/
(function () {
    async function changeAtributePlayer(idUser, idSave, idPlayer, idAtr, val){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/players/"+idPlayer+"/atributos/"+idAtr);

        postsRef.set(val, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar atributo!");
            }
        });
    }
    window.MatchDAO.changeAtributePlayer = changeAtributePlayer;
})();

/*      changeNotePlayer(idUser, idSave, data)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Id do novo player criado
*/
(function () {
    async function changeNotePlayer(idUser, idSave, idPlayer, val){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/players/"+idPlayer+"/anotacoes");

        postsRef.set(val, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar atributo!");
            }
        });
    }
    window.MatchDAO.changeNotePlayer = changeNotePlayer;
})();

/*      getAllNPCs(idUser, idSave)
*  Entrada: id do usuario logado e id do jogo salvo
*  Saida: Lista dos players do jogo salvo em questao do usuario logado
*/
(function () {
    async function getAllNPCs(idUser, idSave){
        let save = await MatchDAO.getSaveById(idUser, idSave);
        let npcs = [];
        for (let a in save.npcs) {
            let b = {id: a};
            npcs.push(Object.assign({},b,save.npcs[a]));
        }
        return npcs;
    }
    window.MatchDAO.getAllNPCs = getAllNPCs;
})();

/*      getNPC(idHistoria, idNPC)
*  Entrada: Id da historia e id do NPC procurado
*  Saida: Informacoes do NPC procurado
*/
(function () {
    async function getNPC(idUser, idSave, idNPC) {
        let npcs = await MatchDAO.getAllNPCs(idUser, idSave);

        if (idNPC > npcs.length)
            return null;
        else
            return npcs[idNPC];
    }
    window.MatchDAO.getNPC = getNPC;
})();

/*      changeAtributeNPC(idUser, idSave, data)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Id do novo player criado
*/
(function () {
    async function changeAtributeNPC(idUser, idSave, idNPC, idAtr, val){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/npcs/"+idNPC+"/atributos/"+idAtr);

        postsRef.set(val, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar atributo!");
            }
        });
    }
    window.MatchDAO.changeAtributeNPC = changeAtributeNPC;
})();

/*      changeAtualNPC(idUser, idSave, data)
*  Entrada: id do usuario logado e id do jogo salvo deste usuario
*  Saida: Id do novo player criado
*/
(function () {
    async function changeAtualNPC(idUser, idSave, val){
        let postsRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/noAtual");

        postsRef.set(val, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar no atual!");
            }
        });
    }
    window.MatchDAO.changeAtualNPC = changeAtualNPC;
})();

/*      changeEstadoGrafo(estado)
*  Entrada: id do usuario logado, id do jogo salvo e o estado a ser salvo
*/
(function () {
    async function changeEstadoGrafo(idUser, idSave, estado){
        let estadoRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/estadoGrafo");

        estadoRef.set(estado, (error) => {
            if (error) {
                console.error(error);
                alert("Nao foi possivel alterar estado atual!");
            }
        });
    }
    window.MatchDAO.changeEstadoGrafo = changeEstadoGrafo;
})();

/*      getEstadoGrafo()
*  Entrada: id do usuario logado e id do jogo salvo
*  Saida: estado salvo
*/
(function () {
    async function getEstadoGrafo(idUser, idSave){
        let estadoRef = firebase.database().ref('usuarios/'+idUser+'/saves/'+idSave+"/estadoGrafo");

        return estadoRef.once('value').then( (snapshot) =>{
            return snapshot.val();
        });

    }
    window.MatchDAO.getEstadoGrafo = getEstadoGrafo;
})();
